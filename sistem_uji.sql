-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2018 at 12:46 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_uji`
--

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_kecerdasan_1`
--

CREATE TABLE `jawaban_kecerdasan_1` (
  `id` int(11) NOT NULL,
  `jawaban` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban_kecerdasan_1`
--

INSERT INTO `jawaban_kecerdasan_1` (`id`, `jawaban`) VALUES
(1, 'Di poskan'),
(2, 'Memberitahukan kepada penjaga bioskop atau satpam tanpa menimbulkan panik'),
(3, 'Agar dipercaya oleh orang lain'),
(4, 'Untuk membantu pemerintah'),
(5, 'Tahan lama, luwes, enak dipakai, dan hemat'),
(6, 'Memanfaatkan keadaan alam disekitar, yang berarti sebagai petunjuk mendapatkan jalan keluar'),
(7, 'Mencoba untuk mendapatkannya'),
(8, 'Peraturan bermasyarakat'),
(9, 'menyebutkan kualitas-kualitas seperti: a. Agar bersih; b. Agar nyamuk tidak bersarang; c. Menghindari banjir'),
(10, 'Untuk menjamin peersamaan hak dan kewajiban suami dan istri');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_kepribadian_daya_juang`
--

CREATE TABLE `jawaban_kepribadian_daya_juang` (
  `id` varchar(255) NOT NULL,
  `tindakan_1` varchar(255) NOT NULL,
  `skor_1` varchar(255) NOT NULL,
  `skor_2` varchar(255) NOT NULL,
  `skor_3` varchar(255) NOT NULL,
  `skor_4` varchar(255) NOT NULL,
  `skor_5` varchar(255) NOT NULL,
  `tindakan_2` varchar(255) NOT NULL,
  `id_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban_kepribadian_daya_juang`
--

INSERT INTO `jawaban_kepribadian_daya_juang` (`id`, `tindakan_1`, `skor_1`, `skor_2`, `skor_3`, `skor_4`, `skor_5`, `tindakan_2`, `id_soal`) VALUES
('10a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 10),
('10b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 10),
('11a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 11),
('11b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 11),
('12a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 12),
('12b', 'Saya', '', '', '', '', '', 'Orang lain atau faktor lain', 12),
('13a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 13),
('13b', 'Bukan tanggung jawab saya', '', '', '', '', '', 'Tanggung jawab saya sepenuhnya', 13),
('14a', 'Tidak bisa saya kendalikan ', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 14),
('14b', 'Saya', '', '', '', '', '', 'Orang lain atau faktor lain', 14),
('15a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 15),
('15b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 15),
('16a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 16),
('16b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 16),
('17a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 17),
('17b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 17),
('18a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 18),
('18b', 'Bukan tanggung jawab saya', '', '', '', '', '', 'Tanggung jawab saya sepenuhnya', 18),
('19a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 19),
('19b', 'Bukan tanggung jawab saya', '', '', '', '', '', 'Tanggung jawab saya sepenuhnya', 19),
('1a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 1),
('1b', 'Saya', '', '', '', '', '', 'Orang lain atau faktor lain', 1),
('20a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 20),
('20b', 'Saya', '', '', '', '', '', 'Orang lain atau faktor lain', 20),
('2a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 2),
('2b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah pernah ada lagi', 2),
('3a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 3),
('3b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 3),
('4a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 4),
('4b', 'Bukan tanggung jawab saya', '', '', '', '', '', 'Tanggung jawab saya sepenuhnya', 4),
('5a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan dengan situasi ini saja', 5),
('5b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 5),
('6a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 6),
('6b', 'Saya', '', '', '', '', '', 'Orang lain atau faktor lain', 6),
('7a', 'Tidak bisa saya kendalikan', '', '', '', '', '', 'Bisa saya kendalikan sepenuhnya', 7),
('7b', 'Bukan tanggung jawab saya', '', '', '', '', '', 'Tanggung jawab saya sepenuhnya', 7),
('8a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 8),
('8b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 8),
('9a', 'Berkaitan dengan aspek kehidupan saya', '', '', '', '', '', 'Berkaitan dengan situasi ini saja', 9),
('9b', 'Akan selalu ada', '', '', '', '', '', 'Tidak akan pernah ada lagi', 9);

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_kepribadian_sifat`
--

CREATE TABLE `jawaban_kepribadian_sifat` (
  `id_uji` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jawaban_positif` varchar(50) NOT NULL,
  `jawaban_negatif` varchar(255) NOT NULL,
  `jumlah_a` int(3) NOT NULL,
  `jumlah_b` int(3) NOT NULL,
  `jumlah_c` int(3) NOT NULL,
  `jumlah_d` int(3) NOT NULL,
  `id_kepribadian` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban_kepribadian_sifat`
--

INSERT INTO `jawaban_kepribadian_sifat` (`id_uji`, `username`, `jawaban_positif`, `jawaban_negatif`, `jumlah_a`, `jumlah_b`, `jumlah_c`, `jumlah_d`, `id_kepribadian`) VALUES
(1, 'wicky372', 'acdbcbcabc-bcacdbaca', 'cdbbbccbbbdcbdbbcddd', 5, 14, 12, 8, 2),
(2, 'qwerty', '--------------------', '--------------------', 0, 0, 0, 0, 1),
(3, 'xffz958', '--------------------', '--------------------', 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kepribadian`
--

CREATE TABLE `kepribadian` (
  `id` int(5) NOT NULL,
  `kepribadian` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepribadian`
--

INSERT INTO `kepribadian` (`id`, `kepribadian`) VALUES
(1, 'Sanguinin Populer'),
(2, 'Kholeris Kuat'),
(3, 'Melankolis Sempurna'),
(4, 'Phlegmatis Damai');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `passHashed` varchar(32) NOT NULL DEFAULT '827ccb0eea8a706c4c34a16891f84e7b',
  `nama` varchar(50) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `id_role` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`username`, `password`, `passHashed`, `nama`, `tempat_lahir`, `tanggal_lahir`, `email`, `id_role`) VALUES
('asd', 'asd', '7815696ecbf1c96e6894b779456d330e', 'Zahra', 'Bogor', '2001-10-07', 'zahra21@gmail.com', 2),
('cyanotic', '12345', '827ccb0eea8a706c4c34a16891f84e7b', 'Cyanotic Admin', 'Bogor', '1995-10-23', 'cyanotic@gmail.com', 1),
('JFMylIH', 'tng5o6s', 'c14b89b49323edcb6f59b9d5e41e4a3f', 'Ilham', 'Bekasi', '1996-07-06', 'ilham.372@yahoo.com', 2),
('ogtr859', 'tTds10C', 'c4b4c1ac6bca91955d3c8b6719ddf14c', 'Roberto Honggo', 'Jonggol', '1996-12-25', 'robert@ymail.com', 2),
('qwerty', 'asdfgh', 'a152e841783914146e4bcd4f39100686', 'Thoriq Putra Kusharta', 'Jakarta', '1996-04-20', 'tayuyaharashi@yahoo.co.id', 2),
('wicky372', 'asdpoi123', 'e452578d5a3150ce02fab080538e72ee', 'Mohamad Ilham', 'Bogor', '1995-11-29', 'ilham.372@gmail.com', 2),
('xffz958', 'pbkuPAb', 'edb79659fba5395e840117afdaab91f7', 'Falaq Putra', 'Bogor', '1998-08-03', 'falaq@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(5) NOT NULL,
  `nama_role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `nama_role`) VALUES
(1, 'Admin'),
(2, 'Pengguna');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_1`
--

CREATE TABLE `soal_kecerdasan_1` (
  `id` int(11) NOT NULL,
  `soal` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_1`
--

INSERT INTO `soal_kecerdasan_1` (`id`, `soal`) VALUES
(1, 'Apa yang saudara lakukan kalau saudara menemukan sebuah amplop di jalan, yang tertutup dan beralamat yang jelas serta berperangko baru yang cukup?'),
(2, 'Apa yang akan saudara lakukan, kalau saudara sedang dalam gedung bioskop dan saudara adalah orang yang pertama yang melihat timbulnya permulaan kebakaran disitu?'),
(3, 'Mengapa janji itu harus ditepati?'),
(4, 'Mengapa kita wajib membayar pajak?'),
(5, 'Sebutkan semua alasan mengapa sepatu itu biasanya dibuat dari kulit?'),
(6, 'Kalau saudara tersesat di dalam hutan sewaktu siang hari, bagaiaman saudara berusaha mencari jalan keluar?'),
(7, 'Apa yang saudara lakukan, kalau sahabat karib saudara meminta sesuatu kepada saudara yang tidak saudara punyai?'),
(8, 'Apa sebab hukum itu perlu?'),
(9, 'Mengapa air di parit (selokan) harus tetap mengalir?'),
(10, 'Mengapa pemerintah menentukan perlu adanya undang-undang perkawinan?');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_2`
--

CREATE TABLE `soal_kecerdasan_2` (
  `id` int(11) NOT NULL,
  `soal` varchar(255) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL,
  `tipe_c` varchar(255) NOT NULL,
  `tipe_d` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_2`
--

INSERT INTO `soal_kecerdasan_2` (`id`, `soal`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`) VALUES
(1, 'Karate adalah semacam sport yang berasal dari .....', 'Spanyol', 'Jepang', 'Amerika', 'Philipina'),
(2, 'Cut Mutia adalah .....', 'Penyanyi', 'Pahlawan Wanita', 'Burung', 'Ikan'),
(3, 'Bahasa Tagalog adalah .....', 'Bahasa Kuno', 'Bahasa Spanyol', 'Bahasa Philipina', 'Bahasa Laos'),
(4, 'Perdata adalah istilah dalam .....', 'Perdagangan', 'Kehakiman', 'Pembangunan', 'Pertunjukkan'),
(5, 'Bapak TNI pada waktu revolusi fisik adalah .....', 'Oerip Soemoharjo', 'Soedirman', 'Adi Soecipto', 'Gatot Soebroto'),
(6, 'Istilah know digunakan untuk mengukur .....', 'Arus Listrik', 'Persenyawaan Kimia', 'Kecepatan Kapal', 'Dalamnya Laut'),
(7, 'Dalam dunia perfilman sering diadakan .....', 'Hadiah Nobel', 'Hadiah Pulitzer', 'Hadiah Oscar', 'Hadiah Davis Cup'),
(8, 'Gajah Mada ialah ahli negara dari .....', 'Majapahit', 'Mataram', 'Singosari', 'Demak'),
(9, 'Raden Saleh terkenal sebagai .....', 'Tokoh Politik', 'Pelukis', 'Burung', 'Ikan'),
(10, 'Tarian Kecak berasal dari .....', 'Bali', 'Jawa', 'Aceh', 'Ambon'),
(11, 'Diantara yang terkenal sebagai Sang pembinasa adalah .....', 'Zarathustra', 'Vishu', 'Arjuna', 'Shiwa'),
(12, 'Mendel terkenal dalam lapangan .....', 'Pendidikan', 'Kesusasteraan', 'Biologi', 'Musik'),
(13, 'Stethoscope dipakai dalam lapangan .....', 'Perindustrian', 'Physika', 'Pertandingan', 'Kedokteran'),
(14, 'Visa adalah .....', 'Mata Uang', 'Buah-buahan', 'Bangunan', 'Surat Izin Masuk Negara'),
(15, 'Klarinet adalah .....', 'Mainan Anak-anak', 'Pedang', 'Alat Musik', 'Alat Tulis'),
(16, 'Kubah terdapat di atas .....', 'Pyramid', 'Mesjid', 'Candi', 'Pura'),
(17, 'Revola In Paradise karya .....', 'Moh. Yamin', 'Rusian Abdul Gani', 'Cindy Adama', 'Ktut Tantri'),
(18, 'Kayu yang mempunyai berat jenis terkecil adalah .....', 'Kayu Balsa', 'Kayu Jati', 'Kayu Damkar', 'Kayu Duren'),
(19, 'Penyakit TBC disebabkan oleh .....', 'Virus', 'Basil', 'Bakteri', 'Racun'),
(20, 'Yang disebut mimosa adalah .....', 'Binatang', 'Tumbuh-tumbuhan', 'Jamur', 'Obat-obatan'),
(21, 'Alpen yang dikunjungi di .....', 'Swedia', 'Rusia', 'Denmark', 'Swiss'),
(22, 'Polka adalah semacam .....', 'Rommel', 'Montgomery', 'George Marshall', 'Eisenhower'),
(23, 'Polka adalah semacam .....', 'Perlombaan Lari', 'Pakaian', 'Tarian', 'Minuman Keras'),
(24, 'Kerajaan Inca terletak di .....', 'Peru', 'Arab', 'Mexico', 'Spanyol'),
(25, 'Dior adalah nama dari .....', 'Semacam Sutra', 'Ahli Negara', 'Pencipta Mode', 'Pemberontak'),
(26, 'Caruso adalah .....', 'Penyanyi', 'Sastrawan', 'Penari', 'Tempat Darmawisata'),
(27, 'Sprinter adalah .....', 'Alat Cetak', 'Pelari Cepat', 'Alat Pertanian', 'Percikan Api'),
(28, 'Bukarest adalah ibukota .....', 'Polandia', 'Rumania', 'Bulgaria', 'Yugoslavia'),
(29, 'Tor-tor berasal dari .....', 'Timor', 'Maluku', 'Sumatera', 'Bali'),
(30, 'Yang menemukan virus penyakit anjing adalah .....', 'Salk', 'Koch', 'Paviov', 'Pasteur'),
(31, 'Spike dipakai untuk .....', 'Atletik', 'Alat Teknik', 'Alat Pertanian', 'Percikan Api'),
(32, 'Safir adalah .....', 'Pengembara', 'Gurun', 'Ahli Filsafat', 'Batu-batuan'),
(33, 'Jedah adalah nama dari .....', 'Makanan', 'Istana', 'Mesjid', 'Pelabuhan'),
(34, 'Tadju Salatin adalah .....', 'Syair', 'Alat Musik', 'Nama Seorang Pujangga', 'Permata'),
(35, 'Triga Mark terdapat di .....', 'Jerman', 'Swiss', 'Denmark', 'Indonesia'),
(36, 'Omar Kayam adalah nama .....', 'Putri India', 'Syair Kuno', 'Pujangga', 'Permata'),
(37, 'Caltex adalah .....', 'Perusahaan Textil', 'Perusahaan Minyak', 'Sejenis Karet', 'Obat-obatan'),
(38, 'Paganini termashur sebagai pemain .....', 'Sandiwara', 'Ballet', 'Biola', 'Sirkus'),
(39, 'Affandi adalah pelukis beraliran .....', 'Naturalis', 'Impressionis', 'Futuris', 'Surrealis'),
(40, 'Spaggeti adalah nama .....', 'Makanan Italia', 'Ahli Filsafat', 'Raja Portugal', 'Tumbuh-tumbuhan');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_3`
--

CREATE TABLE `soal_kecerdasan_3` (
  `id` int(11) NOT NULL,
  `soal` varchar(255) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL,
  `tipe_c` varchar(255) NOT NULL,
  `tipe_d` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_3`
--

INSERT INTO `soal_kecerdasan_3` (`id`, `soal`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`) VALUES
(1, 'Nuri : Burung = Sepat : ?', 'Mangkuk', 'Ikan', 'Aquarium', 'Merah'),
(2, 'Panas : Dingin = Suka : ?', 'Bekerja', 'Tertawa', 'Duka', 'Getir'),
(3, 'Putera : Puteri = Dewa : ?', 'Bidadari', 'Kayangan', 'Dewi', 'Resi'),
(4, 'Kapten : Kapal = Pilot : ?', 'Udara', 'Cepat', 'Pemboman', 'Kapal Terbang'),
(5, 'Wanita : Kebaya = Pria : ?', 'Sepatu', 'Baju', 'Topi', 'Jas'),
(6, 'Makan : Gemuk = Kelaparan : ?', 'Makan', 'Kurus', 'Mati', 'Dahaga'),
(7, 'Mobil : Bensin = Kereta : ?', 'Perlahan-lahan', 'Kusir', 'Mengendarai', 'Kuda'),
(8, 'Gerobak : Roda = Kacamata : ?', 'Lensa', 'Hidung', 'Melihat', 'Teropong'),
(9, 'Siang : Matahari = Malam : ?', 'Gelap', 'Bintang', 'Jauh', 'Tidur'),
(10, 'Menjahit : Jarum = Menebang : ?', 'Pisau', 'Daging', 'Pohon', 'Kampak'),
(11, 'Teman : Kenalan = Marah : ?', 'Ganas', 'Bengis', 'Murka', 'Berbahaya'),
(12, 'Besar : Lebih Besar = Lebih Besar : ?', 'Raksasa', 'Besar', 'Lebih Besar', 'Paling Besar'),
(13, 'Menggerutu : Memaki = Membohong : ?', 'Buruk', 'Penipu', 'Pembual', 'Menghukum'),
(14, 'Es : Dingin = Air : ?', 'Basah', 'Minum', 'Sungai', 'Kering'),
(15, 'Kuda : Cepat = Siput : ?', 'Merayap', 'Lambat', 'Berlendir', 'Berumah'),
(16, 'Beras : Padi = Kacang : ?', 'Hijau', 'Buah Polong-polongan', 'Tanah', 'Panjang'),
(17, 'Saya : Kami = Ia : ?', 'Kita', 'Mereka', 'Kalian', 'Engkau'),
(18, 'Minum : Dahaga = Bernafas : ?', 'Sesak', 'Paru-paru', 'Kalian', 'Engkau'),
(19, 'Enam : Delapan Belas = Dua : ?', 'Delapan', 'Empat', 'Dua', 'Enam'),
(20, 'Busur : Panah = Senapan : ?', 'Peluru', 'Laras', 'Tembakan', 'Letusan'),
(21, 'Bangau : Katak = Ular : ?', 'Reptil', 'Bisa', 'Tikus', 'Melingkar'),
(22, 'Gelas : Susu = Piring : ?', 'Batu', 'Bundar', 'Bubur', 'Makan'),
(23, 'Ayam : Berkokok = Kambing : ?', 'Memanggil', 'Mengembik', 'Padang Rumput', 'Domba'),
(24, 'Kutilang : Burung = Mujahir : ?', 'Air', 'Hasil Penangkapan', 'Ikan', 'Umpan'),
(25, 'Kelakuan : Laku = Perjalanan : ?', 'Menjalankan', 'Berjalan-jalan', 'Jalan', 'Berjalan'),
(26, 'Rumah : Batu = Tungku : ?', 'Pipa', 'Panas', 'Cerobong', 'Besi'),
(27, 'Matahari : Bulat = Balok : ?', 'Panjang', 'Berat', 'Kayu', 'Bundar'),
(28, 'Cemara : Pohon = Putih : ?', 'Merah', 'Susu', 'Warna', 'Bunga'),
(29, 'Menyakiti : Menyayangi = Melukai : ?', 'Sakit', 'Luka', 'Buruk', 'Merawati'),
(30, 'Kenari : Burung = Kemarahan : ?', 'Kerusakan', 'Perasaan', 'Orang', 'Berang'),
(31, 'Bersalah : Hukuman = Jasa : ?', 'Tanda-tanda', 'Anugerah', 'Uang', 'Pekerja'),
(32, 'Es : Air = Air : ?', 'Mendidih', 'Uap', 'Es', 'Basah'),
(33, 'Jam Tangan : Waktu = Thermometer : ?', 'Iklim', 'Derajat', 'Suhu', 'Cuaca'),
(34, 'Kanvas : Melukis = Tanah Liat : ?', 'Patung', 'Kesenian', 'MEmbentuk', 'Esthetic'),
(35, 'Immigrant : Tiba = Emigrasi : ?', 'Pergi', 'Emigrant', 'Asing', 'Penduduk'),
(36, 'Physologi : Ilmu = Hukum : ?', 'Ahli Hukum', 'Pengadilan', 'Jabatan', 'Undang-undang'),
(37, 'Keuntungan : Penjualan = Kemasyuran : ?', 'Pembelian', 'Penipuan', 'Keberanian', 'Jenderal'),
(38, 'Bulan : Bumi = Bumi : ?', 'Mars', 'Bulan', 'Orbit', 'Matahari'),
(39, 'Kesulitan : Kelalaian = Respons : ?', 'Jawaban-jawaban', 'Stimuli', 'Effek', 'Baik'),
(40, 'Kubus : Pyramid = Empat Persegi : ?', 'Peti', 'Mesir', 'Pentagon', 'Segitiga');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_4`
--

CREATE TABLE `soal_kecerdasan_4` (
  `id` int(11) NOT NULL,
  `Soal` varchar(255) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL,
  `tipe_c` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_4`
--

INSERT INTO `soal_kecerdasan_4` (`id`, `Soal`, `tipe_a`, `tipe_b`, `tipe_c`) VALUES
(1, 'Pada beberapa perayaan kita harus memasang bendera, karena:', 'Kita hendak menunjukkan kegembiraan kita.', 'Kita hendak mencegah supaya bendera itu jangan dimakan ngengat.', 'Karena semua orang memasang bendera.'),
(2, 'Mengapa susu lebih bermanfaat untuk kesehatan daripada air:', 'Karena dalam susu terdapat lebih banyak zat-zat yang berguna.', 'Karena susu lebih enak.', 'Karena air lebih lekas menyejukkan.'),
(3, 'Kuda adalah hewan yang berguna:', 'Karena mereka dapat mengerjakan pekerjaan-pekerjaan yang berat untuk kita.', 'Karena kuda tampaknya gagah.', 'Karena mereka makan rumput.'),
(4, 'Jika seorang yang banyak hutangnya menarik undian sebesar Rp. 10 juta, maka ia harus:', 'Mengadakan pesta besar untuk orang-orang yang memberi hutan padanya.', 'Segera membeli sebuah mobil.', 'Segera melunasi semua hutangnya.'),
(5, 'Mengapa kita menghargai orang yang banyak bekerja dan sedikit bicara daripada sebaliknya:', 'Karena orang tak boleh bicara selama bekerja.', 'Karena bekerja lebih penting daripada bicara.', 'Karena berbicara itu tak hormat.'),
(6, 'Binatang-binatang di daerah kutub mempunyai bulu yang tebal karena:', 'Daripadanya dapat di buat mantel dengan kwalitet yang paling tinggi.', 'Jika tidak mereka tidak tahan hawa yang dingin.', 'Kulit yang tipis lekas menjadi basah.'),
(7, 'Mengapa topi-topi di buat dari vilt:', 'Supaya tidak mudah terbang tertiup angin.', 'Karena vilt terbuat dari rambut.', 'Karena vilt ringan dan kuat.'),
(8, 'Mengapa es dapat terapung di atas limun:', 'Karena limun mengandung gula.', 'Karena es itu lebih ringan dari limun.', 'Karena berat jenis es lebih kecil dari limun.'),
(9, 'Mengapa prajurit-prajurit harus memakai sepatu yang berpaku:', 'Karena lebih bagus', 'Karena lebih baik dapat menjaga kaki supaya jangan basah.', 'Supaya tak lekas rusak.'),
(10, 'Mengapa tidak ada kehidupan di bulan:', 'Karena tak ada atmosfir.', 'Karena bulan lambat sekali berputar pada sumbunya.', 'Karena gunung-gunung di sana terlampau tinggi.'),
(11, 'Uap yang terhembus dari ketel air yang di panasi berwarna putih karena:', 'Uap itu berubah menjadi salju.', 'Uap itu selalu putih.', 'Uap akan berkodensasi menjadi titik air.'),
(12, 'Mengapa ada beberapa orang yang merasa takut untuk bepergian dengan kapal terbang:', 'Karena perjalanan dengan kapal terbang sangat mahal.', 'Karena mereka takut akan terjadi kecelakaan dengan kapal terbang.', 'Karena kapal terbang membutuhkan bensin lebih banyak daripada mobil.'),
(13, 'Kendaraan-kendaraan selalu berjalan disebelah kiri jalan karena:', 'Kendaraan-kendaraan dari berbagai jurusan harus disalurkan ke jurusan-jurusan tersendiri.', 'Hal itu merupakan kebiasaan.', 'Kalau tidak akan sukar sekali membelok ke kanan.'),
(14, 'Kalau tanaman menjadi layu karena tidak jatuh hujan, saudara akan:', 'Mengairinya.', 'Minta nasehat kepada tukang-tukang kembang.', 'Memberi pupuk disekitar tanaman itu.'),
(15, 'Jika bulan lebih dekat letaknya ke bumi, maka:', 'Tak akan ada gerhana bulan.', 'Waktu beredar bulan akan menjadi lebih pendek.', 'Orang tak akan melihat bintang-bintang.'),
(16, 'Bila seorang pedagang mengembalikan uang saudara terlampau banyak apakah yang saudara buat:', 'Belikan gula-gula dengan uang itu kepadanya.', 'Berikan uang itu kepada orang miskin yang pertama saudara jumpai.', 'Katakan kepadanya tentang kesalahannya.'),
(17, 'Mengapa kapal-kapal perang diberi warna abu-abu:', 'Karena warna abu-abu lebih murah daripada warna lain.', 'Karena hal itu memberi pekerjaan bagi guru-guru.', 'Karena warna abu-abu membuat kapal-kapal itu lebih sukar untuk dilihat.'),
(18, 'Mengapa pendidikan yang baik itu berguna:', 'Karena hail itu membuat seseorang lebih berguna dan berbahagia.', 'Karena hal itu memberi pekerjaan bagi guru-guru.', 'Karena hal itu akan mengharuskan dibangunkannya sekolah-sekolah dan universitas-universitas.'),
(19, 'Mengapa orang-orang tua (ayah,ibu) diharuskan mengirimkan anak-anaknya ke sekolah:', 'Karena hal itu mempersiapkan mereka untuk kehidupan dewasa.', 'Karena hal itu menjauhkan mereka dari kenakalan.', 'Karena mereka masih terlampau muda untuk bekerja.'),
(20, 'Burung-burung bernyanyi pada musim semi untuk.', 'Memberi tahu kita bahwa musim semi telah tiba.', 'Menarik perhatian pasangannya.', 'Melatih suaranya.');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_5`
--

CREATE TABLE `soal_kecerdasan_5` (
  `id` int(11) NOT NULL,
  `soal` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_5`
--

INSERT INTO `soal_kecerdasan_5` (`id`, `soal`) VALUES
(1, 'Kalau saya berjalan ke muka 8 M. dan mundur 4 M. berapa M.-kah jarak saya dari titik semula ?'),
(2, 'Berapakah jumlah 47 orang dan 9 orang ?'),
(3, 'Amat mempunyai Rp. 19,- , Ia menerima lagi Rp. 7,- dan mengeluarkan Rp. 13,- . Berapakah sisa uangnya ?'),
(4, 'Seseorang membelanjanjakkan sepersembilan dari uangnya untuk membeli kertas tulis dan 5 kali daripadanya itu untuk membeli perangko. Sisanya adalah Rp. 4,50. Berapa jumlah uang semulanya ?'),
(5, 'Kalau 6 orang harus membagi 480 Rupiah, berapa Rupiah yang akan diterima oleh masing-masing orang?'),
(6, 'Saya membeli 3 butir telor dengan Rp. 5,- sebutir, dan 1 kg gula seharga Rp. 21,- . Saya membayar Rp. 100,- . Berapakah yang akan saya terima kembali ?'),
(7, 'Berapa batang rokok dapat saudara beli dengan uang 16 Rupiah, kalau dengan 4 Rupiah saudara mendapat 3 batang ?'),
(8, 'Seorang pengedara sepeda motor menempuh 500 km dalam 5 hari. Hari yang pertama ia menempuh 90 km, hari kedua 75 km, hari ketiga 120 km, dan hari yang keempat 30 km. Berapa km, yang ditempuhnya pada hari kelima ?'),
(9, 'Berapa jam akan ditempuh oleh sebuah kereta yang kecepatannya 70 km sejam dengan panjang jalan 350 km ?'),
(10, 'Kalau saudara mendapat uang Rp. 25,- dalam 1 jam, berapa akan diperoleh dalam 7 jam ?'),
(11, 'Kalau 7 1/2 kg gula merah harganya Rp. 90,- , berapa harga 2 1/2 kg ?'),
(12, 'Sebuah bak persegi panjang, panjangnya 4m dan lebarnya 3m, serta isinya 40 m3. Berapa dalamnya bak itu ?'),
(13, 'Empat orang menggali selokan selesai selama 7 hari. Berapa orang yang diperlukan untuk menggali selokan itu dalam waktu 1/2 hari ?'),
(14, 'Seorang peladang membeli beberapa anak sapi dengan harga Rp. 6 Juta ia menjualnya dengan harga Rp. 7,5 Juta dan mendapat keuntungan 300 Ribu Rupiah untuk tiap-tiap anak sapi. Berapa ekor anak sapi yang dibelinya ?'),
(15, 'Sebuah pabrik menyediakan arang batu untuk memanaskan enam buah ketel dalam empat minggu. Berapa lamakah pabrik itu harus menyediakan arang batu supaya dapat dipakai untuk memanaskan 18 buah ketel ?'),
(16, 'Ibu membeli 3 kg mentega seharga Rp. 135,-. Berapa banyak ibu mendapat mentega dengan Rp. 27,- ?'),
(17, 'Sebuah mobil berjalan dengan kecepatan rata-rata 30 km per jam pada hari yang berkabut, dan di hari yang terang cuacanya rata-rata 60 km per jam. Berapa lama ia lakukan pada jalan yang panjangnya 210 km, kalau 2/7 dari pejalanan itu berkabut ?'),
(18, 'Pipa air di suatu tempat mempunyai 523 cabang saluran. Dalam satu minggu terpakai 8891 liter air. Berapa liter air rata-rata yang dipakai oleh tiap keluarga dalam waktu satu minggu itu ?'),
(19, 'Seorang pembuat jalan harus memasang batu tegel yang panjangnya 6 dm dan lebarnya 60 cm, ia membutuhkan 600 batu tegel. Berapa m2-kah jalan itu ?'),
(20, 'Pada suatu tempat ada 1365 pemuda yang memenuhi syarat untuk dinas tentara, 9% yang tidak memenuhi syarat. Berapa orang yang datang pada pemeriksaan itu ?');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_6`
--

CREATE TABLE `soal_kecerdasan_6` (
  `id` int(11) NOT NULL,
  `tipe_1` varchar(11) NOT NULL,
  `tipe_2` varchar(11) NOT NULL,
  `tipe_3` varchar(11) NOT NULL,
  `tipe_4` varchar(11) NOT NULL,
  `tipe_5` varchar(11) NOT NULL,
  `tipe_6` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_6`
--

INSERT INTO `soal_kecerdasan_6` (`id`, `tipe_1`, `tipe_2`, `tipe_3`, `tipe_4`, `tipe_5`, `tipe_6`) VALUES
(1, '4', '8', '12', '16', '20', '24'),
(2, '17', '22', '27', '32', '37', '42'),
(3, '1', '2', '4', '8', '16', '32'),
(4, '21', '19', '17', '15', '13', '11'),
(5, '12', '1', '9', '1', '6', '1'),
(6, '10', '10', '7', '7', '4', '4'),
(7, '16', '8', '4', '2', '1', '1/2'),
(8, '1/3', '1', '3', '9', '27', '81'),
(9, '65', '58', '51', '44', '37', '30'),
(10, '10', '12', '11', '13', '12', '14'),
(11, '5', '6', '8', '11', '15', '20'),
(12, '2', '5', '4', '7', '6', '9'),
(13, '31', '30', '28', '25', '21', '16'),
(14, '7', '6', '12', '11', '17', '16'),
(15, '14', '16', '11', '13', '8', '10'),
(16, '12', '18', '30', '36', '48', '54'),
(17, '19', '15', '18', '14', '17', '13'),
(18, '4', '8', '11', '22', '25', '50'),
(19, '45', '15', '18', '6', '9', '3'),
(20, '22', '28', '14', '20', '10', '16'),
(21, '4', '8', '6', '12', '10', '20'),
(22, '3', '5', '7', '4', '6', '8'),
(23, '1', '2', '6', '24', '120', '720'),
(24, '1', '9', '2', '8', '3', '7'),
(25, '37', '44', '27', '34', '17', '24'),
(26, '8', '3', '4', '6', '2', '12'),
(27, '276', '272', '60', '64', '16', '12'),
(28, '96', '7', '24', '14', '6', '28'),
(29, '180', '171', '153', '126', '90', '45'),
(30, '24', '16', '12', '48/5', '8', '48/7');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_7`
--

CREATE TABLE `soal_kecerdasan_7` (
  `id` int(11) NOT NULL,
  `soal` varchar(255) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL,
  `tipe_c` varchar(255) NOT NULL,
  `tipe_d` varchar(255) NOT NULL,
  `tipe_e` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_7`
--

INSERT INTO `soal_kecerdasan_7` (`id`, `soal`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`, `tipe_e`) VALUES
(1, 'Lawan hemat adalah .....', 'Pantas', 'Kikir', 'Royal', 'Berharga', 'Kaya'),
(2, '..... tidak ada sangkut pautnya dengan hawa.', 'Taufan', 'Badai', 'Hujan', 'Gempa Bumi', 'Kabut'),
(3, 'Kota ..... menghasilkan semen.', 'Jakarta', 'Bogor', 'Padang', 'Manado', 'Medan'),
(4, 'Pengaruh seseorang terhadap sesama manusia harus tergantung dari .....', 'Kekuasaan', 'Kemampuan Orisinil', 'Kekayaan', 'Kemasyuran', 'Kebijaksanaan'),
(5, 'Tinggi seorang laki-laki Indonesia rata-rata .....', '170 cm', '150 cm', '145 cm', '175 cm', '165 cm'),
(6, 'Lawan dari \"Kesetiaan\" adalah .....', 'Kecintaan', 'Kebencian', 'Persahabatan', 'Pengkhianatan', 'Permusuhan'),
(7, 'Seorang yang meragukan akan kemajuan disebut .....', 'Demokratis', 'Radikal', 'Liberal', 'Konservatif', 'Progresif'),
(8, 'Seorang ayah ..... lebih berpengalaman dari anaknya.', 'Selalu', 'Biasanya', 'Jauh', 'Jarang', 'Secara Prinsipil'),
(9, 'Kuda selalu mempunyai .....', 'Kandang', 'Tapak Kuda', 'Pelana', 'Kuku', 'Gigi Taring'),
(10, 'Sepatu selalu mempunyai .....', 'Kulit', 'Sol Sepatu', 'Tali Sepatu', 'Paku', 'Lidah Sepatu'),
(11, 'Seorang paman adalah ..... lebih tua dari kemenakannya.', 'Jarang', 'Sering Kali', 'Selalu', 'Tidak Pernah', 'Kadang-kadang'),
(12, 'Pada suatu pertandingan selalu terdapat .....', 'Wasit', 'Lawan', 'Penonton', 'Tepuk Tangan', 'Kemenangan'),
(13, '..... tidak termasuk alat pencegah kecelakaan lalu-lintas.', 'Lampu Stop', 'Kaca Mata', 'Kotak Obat', 'Tanda Peringatan', 'Palang Jalan Penutup'),
(14, '..... mempunyai nilai kalori yang paling tinggi dalam jumlah yang sama.', 'Ikan', 'Daging', 'Lemak', 'Keju', 'Sayur'),
(15, 'Kalau kita tahu persentase hadiah-hadiah dari suatu undian (lotre), maka kita dapat menghitung .....', 'Jumlah Hadiah-hadiah', 'Pajak dari Undian', 'Kemungkinan Menang', 'Jumlah Pembeli Undian', 'Hadiah Tertinggi'),
(16, 'Lawan dari \"tidak pernah\" adalah .....', 'Sering', 'Pernah', 'Kadang-kadang', 'Banyak Kali', 'Selalu'),
(17, 'Diperlukan banyak ..... untuk menimbulkan nada tinggi.', 'Perasaan', 'Tenaga', 'Amplitudo', 'Frekwensi', 'Laras'),
(18, 'Uang kertas Rp. 100,- seri terbaru kira-kira panjangnya .....', '12 cm', '16 cm', '19 cm', '14,5 cm', '10 cm'),
(19, 'Suatu penjelasan yang belum terbukti kebenarannya dianggap suatu yang .....', 'Paradox', 'Tergesa-gesa', 'Dualitis', 'Salah', 'Hipotesis'),
(20, 'Jarak lurus antara Jakarta - Surabaya adalah .....', '600 km', '800 km', '700 km', '950 km', '1050 km');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kecerdasan_8`
--

CREATE TABLE `soal_kecerdasan_8` (
  `id` int(11) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL,
  `tipe_c` varchar(255) NOT NULL,
  `tipe_d` varchar(255) NOT NULL,
  `tipe_e` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kecerdasan_8`
--

INSERT INTO `soal_kecerdasan_8` (`id`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`, `tipe_e`) VALUES
(1, 'Jurusan', 'Timur', 'Perjalanan', 'Arah', 'Selatan'),
(2, 'Lingkaran', 'Panah', 'Busur', 'Lengkung', 'Ellips'),
(3, 'Mengetok', 'Makanan', 'Menjahit', 'Menggergaji', 'Memalu'),
(4, 'Bus', 'Tram', 'Sepeda Motor', 'Sepeda', 'Kereta Api'),
(5, 'Mengikat', 'Menghubungkan', 'Melepaskan', 'Menjalin', 'Menempel'),
(6, 'Memuai', 'Besar', 'Keliling', 'Kelangsungan', 'Isi'),
(7, 'Terbang', 'Jalan', 'Berlayar', 'Bersepeda', 'Berkuda'),
(8, 'Batu', 'Baja', 'Wol', 'Karet', 'Kayu'),
(9, 'Biola', 'Suling', 'Terompet', 'Klarinet', 'Saksofon'),
(10, 'Bergelombang', 'Kasar', 'Berbenjol', 'Licin', 'Lurus'),
(11, 'Pembagi', 'Perpisahan', 'Lorong', 'Perbatasan', 'Pembelahan'),
(12, 'Foto', 'Patung', 'Silhouette', 'Relief', 'Lukisan'),
(13, 'Kompas', 'Jam', 'Penunjuk Jalan', 'Bintang Utara', 'Arah'),
(14, 'Jembatan', 'Perbatasan', 'Perkawinan', 'Jalan Setapak', 'Kekeluargaan'),
(15, 'Ruitsluiting', 'Palang Pintu', 'Kran Air', 'Obeng', 'Botol'),
(16, 'Oval', 'Panjang', 'Runcing', 'Bulat', 'Bersegi-segi'),
(17, 'Rokok', 'Kopi', 'Limun', 'Bir', 'Madat'),
(18, 'Mengkilat', 'Halus', 'Buram', 'Licin', 'Berkilauan'),
(19, 'Latihan', 'Rencana', 'Belajar', 'Putusan', 'Propaganda'),
(20, 'Mengetam', 'Mengebor', 'Mengasah', 'Mengoles', 'Menyetrika');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kepribadian_1`
--

CREATE TABLE `soal_kepribadian_1` (
  `id` int(11) NOT NULL,
  `tipe_a` varchar(50) NOT NULL,
  `tipe_b` varchar(50) NOT NULL,
  `tipe_c` varchar(50) NOT NULL,
  `tipe_d` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kepribadian_1`
--

INSERT INTO `soal_kepribadian_1` (`id`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`) VALUES
(1, 'Asyik', 'Berani', 'Analitis', 'Menyesuaikan'),
(2, 'Suka Melucu', 'Meyakinkan', 'Keras Hati', 'Tenang'),
(3, 'Mau Berkorban', 'Berkemauan Kuat', 'Taat Perintah', 'Rendah Hati'),
(4, 'Meyakinkan', 'Bersaing', 'Pertimbangan', 'Perhatian'),
(5, 'Menyegarkan', 'Bertindak Cepat', 'Menghormati', 'Pendiam'),
(6, 'Bersemangat', 'Banyak Akal', 'Perasa', 'Memuaskan'),
(7, 'Penganjur', 'Positif', 'Perencana', 'Sabar'),
(8, 'Spontan', 'Lurus', 'Pemalu', 'Stabil'),
(9, 'Optimis', 'Terus Terang', 'Rapi', 'Penolong'),
(10, 'Lucu', 'Berpendirian Kuat', 'Setia', 'Ramah'),
(11, 'Diplomatis', 'Pekerja Keras', 'Terperinci', 'Konstan'),
(12, 'Periang', 'Suka Hal Pasti', 'Terpelajar', 'Konsisten'),
(13, 'Ceria', 'Merdeka', 'Idealis', 'Tenang'),
(14, 'Lincah', 'Menentukan', 'Mendalam', 'Suka Humor'),
(15, 'Mudah Berbaur', 'Penggerak', 'Berbakat Musik', 'Penengah'),
(16, 'Pembicara', 'Ulet', 'Tenggang Hati', 'Toleran'),
(17, 'Gesit', 'Pemimpin Team', 'Setia', 'Pendengar Baik'),
(18, 'Elok Sekali', 'Kepala Group', 'Pembuat Peta', 'Mudah Puas'),
(19, 'Populer', 'Produktif', 'Ingin Sempurna', 'Menyenangkan'),
(20, 'Bergairah', 'Tegas', 'Berkelakuan Baik', 'Stabil');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kepribadian_2`
--

CREATE TABLE `soal_kepribadian_2` (
  `id` int(50) NOT NULL,
  `tipe_a` varchar(50) NOT NULL,
  `tipe_b` varchar(50) NOT NULL,
  `tipe_c` varchar(50) NOT NULL,
  `tipe_d` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kepribadian_2`
--

INSERT INTO `soal_kepribadian_2` (`id`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`) VALUES
(1, 'Tukang Tiup', 'Suka Merintah', 'Tertutup', 'Hampa'),
(2, 'Tidak Disiplin', 'Tidak Simpatik', 'Tak Ada Ampun', 'Tidak Menarik'),
(3, 'Klise', 'Menentang', 'Kecewa', 'Segan'),
(4, 'Delupa', 'Blak-Blakan', 'Cerewet', 'Takut-Takut'),
(5, 'Suka Menyela', 'Tidak Sabar', 'Gelisah', 'Bimbang'),
(6, 'Sulit Ditebak', 'Tidak Sayang', 'Tidak Populer', 'Tak Terbelit-Belit'),
(7, 'Sembrono', 'Paksa Kehendak', 'Standar Tinggi', 'Ragu-Ragu'),
(8, 'Sesuka Hati', 'Bangga', 'Pesimis', 'Datar'),
(9, 'Mudah Marah', 'Membantah', 'Merasa Asing', 'Tak Ada Tujuan'),
(10, 'Kekanak-Kanakan', 'Suka Nekat', 'Melihat Sisi Buruk', 'Masa Bodo'),
(11, 'Ingin Dipercaya', 'Mabuk Kerja', 'Menarik Diri', 'Selalu Cemas'),
(12, 'Banyak Bicara', 'Kurang Perasa', 'Sensitif', 'Hindari Kesulitan'),
(13, 'Tidak Teratur', 'Mendominasi', 'Merasa Tertekan', 'Tak Percaya Diri'),
(14, 'Plin-Plan', 'Tidak Ada Toleransi', 'Menutup Diri', 'Acuh Tak Acuh'),
(15, 'Kacau Balau', 'Manipulatif', 'Suka Murung', 'Suka Mengomel'),
(16, 'Sok Aksi', 'Tak Mudah Dibujuk', 'Tak Mudah Percaya', 'Lamban'),
(17, 'Suaranya Nyaring', 'Sok Berkuasa', 'Suka Menyendiri', 'Malas'),
(18, 'Angin-Anginan', 'Tempramental', 'Suka Curiga', 'Perlu Dorongan'),
(19, 'Suka Hal Baru', 'Terburu-Buru', 'Suka Dendam', 'Tak Suka Terlibat'),
(20, 'Mudah Bosan', 'Licik', 'Reaksi Negatif', 'Suka Mengalah');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kepribadian_daya_juang`
--

CREATE TABLE `soal_kepribadian_daya_juang` (
  `id` int(11) NOT NULL,
  `peristiwa` varchar(255) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kepribadian_daya_juang`
--

INSERT INTO `soal_kepribadian_daya_juang` (`id`, `peristiwa`, `tipe_a`, `tipe_b`) VALUES
(1, 'Rekan kerja anda tidak menerima ide-ide anda.', 'Yang menyebabkan rekan kerja saya tidak menerima ide saya merupakan sesuatu yang:', 'Penyebab rekan kerja saya tidak menerima ide saya sepenuhnya berkaitan dengan:'),
(2, 'Orang tidak tanggap terhadap presentasi anda disuatu rapat.', 'Yang menyebabkan orang tidak tanggap terhadap presentasi saya adalah sesuatu yang:', 'Penyebab orang tidak tanggap dengan presentasi saya:'),
(3, 'Hubungan anda dengan orang-orang yang anda cintai tampaknya makin jauh.', 'Yang menyebabkan hubungan kami tampaknya makin jauh adalah sesuatu yang:', 'Penyebab hubungan kami yang tampaknya makin jauh:'),
(4, 'Anda bertengkar hebat dengan pasangan hidup Anda (orang lain yang penting).', 'Yang menyebabkan kami bertengkar hebat adalah sesuatu yang:', 'Hasil dari peristiwa ini adalah sesuatu yang saya rasa:'),
(5, 'Anda diminta untuk pindah tempat kalau Anda ingin tetap bekerja.', 'Yang menyebabkan saya diminta untuk pindah tempat adalah sesuatu yang:', 'Penyebab saya diminta untuk pindah tempat:'),
(6, 'Seorang teman karib tidak menelepon pada hari ulang tahun Anda.', 'Yang menyebabkan teman saya tidak menelepon adalah sesuatu yang:', 'Penyebab teman saya tidak menelepon sepenuhnya berkaitan dengan:'),
(7, 'Seorang sahabat karib Anda sakit parah.', 'Yang menyebabkan sahabat saya sakit parah adalah sesuatu yang:', 'Hasil dari peristiwa ini  adalah sesuatu yang saya rasa:'),
(8, 'Anda tidak mendapat penugasan yang penting.', 'Yang menyebabkan saya ditolak untuk penugasan tersebut adalah sesuatu yang:', 'Penyebab saya ditolak untuk penugasan tersebut:'),
(9, 'Anda mendapat umpan balik yang negatif dari seorang teman kerja yang dekat dengan Anda.', 'Yang menyebabkan saya mendapat umpan balik yang negatif adalah sesuatu yang:', 'Penyebab saya mendapat umpan balik negatif itu:'),
(10, 'Sesorang yang dekat dengan Anda didiagnosis menderita kanker.', 'Yang menyebabkan dia menderita kanker adalah sesuatu yang:', 'Penyebab dia mengidap kanker:'),
(11, 'Strategi investasi Anda yang mutakhir mendatangkan kerugian.', 'Yang menyebabkan strategi saya gagal ada sesuatu yang:', 'Penyebab strategi saya gagal:'),
(12, 'Anda ketinggalan pesawat:', 'Yang menyebabkan saya ketinggalan pesawat adalah sesuatu yang:', 'Penyebab saya ketinggalan pesawat sepenuhnya berkaitan dengan:'),
(13, 'Proyek yang Anda tangani gagal.', 'Yang menyebabkan proyek tersebut gagal adalah sesuatu yang:', 'Hasil dari peristiwa ini adalah sesuatu yang saya rasa:'),
(14, 'Majikan Anda menawarkan untuk memotong gaji Anda sebesar 30% kalau Anda tetap ingin bekerja.', 'Yang menyebabkan saya diminta menerima pemotongan gaji adalah sesuatu yang:', 'Penyebab saya diminta menerima pemotongan gaji sepenuhnya berkaitan dengan:'),
(15, 'Dokter Anda memberi tahu bahwa kadar kolesterol Anda terlampau tinggi.', 'Yang menyebabkan kolesterol saya terlampau tinggi adalah sesuatu yang:', 'Penyebab kolesterol saya terlampau tinggi:'),
(16, 'Mobil Anda mogok dalam perjalanan ke sebuah janji pertemuan.', 'Yang menyebabkan mobil saya mogok adalah sesuatu yang:', 'Penyebab mobil saya mogok:'),
(17, 'Anda menelpon seorang teman berkali-kali dan meninggalkan pesan tapi tidak satu pun dibalas.', 'Yang menyebabkan teman saya tidak menjawab telepon saya adalah sesuatu yang:', 'Penyebab teman saya tidak menjawab telepon saya:'),
(18, 'Saat pemeriksaan kesehatan dokter Anda memperingatkan kesehatan Anda.', 'Yang menyebabkan dokter saya memperingatkan saya adalah sesuatu yang:', 'Hasil dari peristiwa ini adalah sesuatu yang saya rasa:'),
(19, 'Hasil penilaian kinerja Anda tidak menyenangkan.', 'Yang menyebabkan saya menerima penilaian seperti itu adalah sesuatu yang:', 'Hasil dari peristiwa ini adalah sesuatu yang saya rasa:'),
(20, 'Anda tidak menerima promosi yang sangat Anda harapkan.', 'Yang menyebabkan saya tidak mendapat promosi adalah sesuatu yang:', 'Penyebab saya tidak mendapat promosi sepenuhnya berkaitan dengan:');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kepribadian_gaya_kepemimpinan`
--

CREATE TABLE `soal_kepribadian_gaya_kepemimpinan` (
  `id` int(11) NOT NULL,
  `situasi` varchar(500) NOT NULL,
  `tipe_a` varchar(255) NOT NULL,
  `tipe_b` varchar(255) NOT NULL,
  `tipe_c` varchar(255) NOT NULL,
  `tipe_d` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kepribadian_gaya_kepemimpinan`
--

INSERT INTO `soal_kepribadian_gaya_kepemimpinan` (`id`, `situasi`, `tipe_a`, `tipe_b`, `tipe_c`, `tipe_d`) VALUES
(1, 'Akhir-akhir ini bawahan anda tidak menanggapi pembicaraan bersahabat dan perhatian anda terhadap kesejahteraan mereka. Penampilan mereka tampak menurun dengan tajam.', 'Menekankan penggunaan prosedur yang seragam dan keharusan penyelesaian tugas.', 'Anda menyediakan waktu untuk berdiskusi, tetapi tidak mendorong keterlibatan anda.', 'Berbicara dengan bawahan dan menyusun tujuan-tujuan.', 'Secara sengaja tidak campur tangan.'),
(2, 'Penampilan kelompok anda tampak meningkat sekarang. Anda merasa yakin bahwa semua anggota menyadari tanggung jawab dan standar penampilan yang diharapkan dari mereka.', 'Melibatkan diri dalam interaksi bersahabat, tetapi harus berusaha memastikan bahwa semua anggota menyadari tanggung jawab dan standar penampilan mereka.', 'Tidak mengambil tindakan apapun.', 'Melakukan apa saja yang dapat anda kerjakan untuk membuat kelompok merasa penting dan dilibatkan.', 'Menekankan pentingnya batas waktu dan tugas-tugas.'),
(3, 'Anggota-anggota kelompok anda tidak dapat memecahkan suatu masalah. Anda biasanya membiarkan mereka bekerja sendiri. Selama ini penampilan kelompok dan hubungan antara anggota adalah baik.', 'Bekerja dengan kelompok dan bersama-sama terlibat dalam pemecahan masalah.', 'Membiarkan kelompok mengusahakan sendiri pemecahannya.', 'Bertindak cepat dan tegas untuk mengoreksi dan mengarahkan kembali.', 'Mendorong kelompok untuk berusaha memecahkan masalah dan mendukung usaha-usaha mereka.'),
(4, 'Anda sedang mempertimbangkan adanya suatu perubahan bawahan anda menunjukkan penampilan yang baik. Mereka menyambut perlunya perubahan dengan baik.', 'Melibatkan kelompok dalam mengembangkan perubahan itu, tetapi jangan terlalu mengarahkan.', 'Mengumumkan perubahan-perubahan dan kemudian menerapkan dengan pengawasan yang cermat.', 'Membiarkan kelompok merumuskan arahnya sendiri.', 'Mengikuti rekomendasi kelompok, tetapi anda yang mengarahkan perubahan.'),
(5, 'Penampilan kelompok anda telah menurun selama beberapa bulan terakhir. Bawahan telah mengabaikan pencapaian tujuan. Penegasan kembali peranan dan pertanggungjawaban telah sangat membantu mengatasi situasi tersebut di masa-masa lalu. Mereka secara terus menerus memerlukan peringatan untuk menyelesaikan tugas tepat pada waktunya.', 'Membiarkan kelompok merumuskan arahnya sendiri.', 'Menyetujui rekomendasi kelompok, tetapi lihat apakah tujuan tercapai.', 'Menegaskan kembali peranan dan tanggung jawab serta melakukan pengawasan dengan cermat.', 'Melibatkan kelompok dalam menetapkan peranan dan tanggung jawab, tetapi tidak terlalu mengarahkan.'),
(6, 'Anda memasuki suatu organisasi yang berjalan secara efisien. Pimpinan sebelumnya mengontrol situasi dengan ketat. Anda ingin mempertahankan situasi yang produktif, tetapi ingin pula membangun lingkungan yang manusiawi.', 'Melakukan apa saja yang dapat anda kerjakan untuk membuat kelompok merasa penting dan dilibatkan', 'Menekankan pentingnya batas waktu dan tugas-tugas.', 'Secara sengaja tidak mengambil tindakan apapun.', 'Mengusahakan keterlibatan kelompok dalam pengambilan keputusan, tetapi lihat apakah tujuan tercapai.'),
(7, 'Anda mempertimbangkan untuk berubah kepada suatu struktur yang baru bagi kelompok anda. Para anggota telah menyampaikan saran-saran mengenai perubahan yang diperlukan. Penampilan kelompok selama ini adalah produktif dan telah mendemonstrasikan keluesan dalam pelaksanaan tugas.', 'Menjelaskan perubahan dan mengawasi dengan cermat.', 'Mengikutsertakan kelompok dalam mengembangkan perubahan, tetapi membiarkan mereka mengorganisasikan penerapanya.', 'Menyetujui adanya perubahan seperti yang direkomendasikan, tetapi mempertahankan pengawasan dalam penerapannya.', 'Membiarkan kelompok sendiri bagaiaman adanya.'),
(8, 'Penampilan kelompok dan hubungan antara anggota adalah baik, anda merasa sedikit ragu-ragu mengenai kurangnya pengarahan anda terhadap kelompok.', 'Membiarkan kelompok sendiri.', 'Mendiskusikan situasi dengan kelompok dan kemudian anda memulai perubahan-perubahan yang perlu.', 'Mengambil langkah-langkah untuk mengarahkan bawahan ke arah pelaksanaan tugas dengan perencanaan yang baik.', 'Bersikap sportif dalam mendiskusikan situasi dengan kelompok, tetapi tidak terlalu mengarahkan.'),
(9, 'Atasan telah menegaskan anda untuk mengepalai suatu satuan tugas yang sangat terlambat dalam membuat rekomendasi bagi perubahan yang diharapkan. Tujuan kelompok tidak jelas. Kehadiran anggota dalam persidangan tidak sebagaimana diharapkan. Pertemuan-pertemuan telah berbalik fungsi menjadi ajang perbincangan antar sejawat. Sebenarnya mereka memiliki kecakapan yang potensial diperlukan untuk membantu.', 'Membiarkan memecahkan persoalanya sendiri.', 'Menyetujui rekomendasi kelompok, tetapi lihat apakah tujuan tercapai.', 'Menegaskan kembali tujuan-tujuan dan mengawasi secara ketat.', 'Membiarkan keterlibatan kelompok dalam penyusunan tujuan, tetapi tidak mendorong.'),
(10, 'Bawahan anda, yang biasanya mampu memikul tanggung jawab, tidak menanggapi penegasan kembali standar yang anda tetapkan baru-baru ini.', 'Membiarkan keterlibatan kelompok dalam menegaskan kembali standar, tetapi tidak melakukan kontrol.', 'Menegaskan kembali standar dan mengawasi dengan seksama.', 'Menghindari konfrontasi dengan tidak melakukan tekanan, biarkan saja situasinya demikian.', 'Mengikuti rekomendasi kelompok, tetapi lihat apakah tujuan tercapai.'),
(11, 'Anda dipromosikan pada posisi baru, pimpinan sebelumnya tidak terlibat dalam persoalan kelompok. Tugas-tugas dan pengarahan kelompok telah ditangani secara memadai. Kelompok tidak mengahadapi masalah dalam hubungan personal.', 'Mengambil langkah-langkah untuk mengarahkan bawahan kearah pelaksanaan tugas dengan perencanaan yang baik.', 'Melibatkan bawahan dalam pengambilan keputusan dan dorongan adanya kontribusi konstruktif.', 'Mendiskusikan penampilan dimasa lampau dengan kelompok dan kemudian anda menguji perlunya praktek-praktek baru.', 'Membiarkan kelompok sebagaimana adanya.'),
(12, 'Informasi terakhir menunjukkan timbulnya ketidakharmonisan diantara bawahan. Kelompok telah memiliki rekor pelaksanaan tugas dengan hasil yang mengagumkan. Para anggota secara efektif telah berpedoman pada tujuan-tujuan jangka panjang. Mereka telah bekerja secara harmonis bertahun-tahun yang lalu. Semua anggota berkualitas baik untuk tugas-tugas mereka.', 'Mengujicobakan pemecahan masalah dengan bawahan dan periksa seperlunya praktek-praktek baru.', 'Membiarkan anggota kelompok memecahkan sendiri persoalannya.', 'Bertindak cepat dan tegas untuk mengoreksi dan pengarahan kembali.', 'Ikut serta dalam mendiskusikan masalah sembari memberikan dukungan kepada bawahan.');

-- --------------------------------------------------------

--
-- Table structure for table `soal_kepribadian_gaya_manajemen`
--

CREATE TABLE `soal_kepribadian_gaya_manajemen` (
  `id` int(11) NOT NULL,
  `tipe_a` varchar(500) NOT NULL,
  `tipe_b` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_kepribadian_gaya_manajemen`
--

INSERT INTO `soal_kepribadian_gaya_manajemen` (`id`, `tipe_a`, `tipe_b`) VALUES
(1, 'Saya tidak memperdulikan pelanggaran atas peraturan apabila saya yakin bahwa tidak ada orang lain yang mengetahui pelanggaran itu.', 'Jika saya mengumumkan suatu keputusan yang tidak menyenangkan, kepada bawahan, saya menerangkan bahwa keputusan itu dibuat sendiri oleh atas saya.'),
(2, 'Bila pekerjaan seorang pegawai terus-menerus tidak memuaskan, saya akan menunggu sampai ada kesempatan untuk memutasikan pegawai itu daripada memecatnya.', 'Jika salah seorang bawahan tidak termasuk kedalam suatu kelompok, saya akan berusaha keras agar orang-orang yang didalam kelompok itu berteman dengan bawahan tersebut.'),
(3, 'Jika atasan memberi suatu instruksi, yang tidak menyenangkan, saya berpendapat bahwa selayaknya bila instruksi yang tidak meneyenangkan itu atas nama atas saya dan bukan atas nama saya.', 'Saya selalu mengambil keputusan-keputusan sendiri, kemudian memberitahu bawahan mengenai keputusan-keputusan itu.'),
(4, 'Kalau saya ditegur oleh atasan, saya memanggil bawahan-bawahan saya dan menyampaikan teguran itu pada mereka.', 'Saya selalu menyarankan pekerjaan-pekerjaan yang paling sulit kepada bawahan-bawahan yang paling berpengalaman.'),
(5, 'Saya acapkali membiarkan suatu diskusi mengarah ke hal-hal yang lain.', 'Saya mendorong bawahan-bawahan untuk mengajukan usul-usul tetapi jarang mengambil tindakan dari usul-usul tersebut.'),
(6, 'Kadang-kadang saya berfikir bahwa perasaan dan sikap diri saya adalah sama penting seperti pekerjaan saya.', 'Saya mengijinkan bawahan untuk turut serta dalam membuat keputusan dan selalu berpegang pada keputusan mayoritas.'),
(7, 'Jika mutu atau jumlah pekerjaan suatu unit kerja yang saya pimpin tidak memuaskan, saya menerangkan kepada bawahan bahwa atasan saya tidak puas serta mereka harus memperbaiki pekerjaan mereka.', 'Saya mengambil keputusan sendiri dan kemudian mencoba menyampaikannya kepada bawahan.'),
(8, 'Jika saya mengumumkan suatu keputusan yang tidak menyenangkan kepada bawahan, saya menerangkan bahwa keputusan itu dibuat sendiri oleh atasan saya.', 'Saya mengijinkan bawahan untuk turut serta dalam membuat keputusan, tetapi keputusan terakhir tetap berada ditangan saya.'),
(9, 'Saya mungkin menyerahkan tugas yang sulit kepada bawahan yang tidak mempunyai pengalaman, tetapi jika mereka mendapat kesulitan dalam mengerjakan pekerjaan itu, saya akan mengambil alih pekerjaan itu dan mengerjakannya sendiri atau dikerjakan oleh orang lain', 'Jika mutu atau jumlah pekerjaan suatu unit yang saya pimpin tidak memuaskan, saya menerangkan kepada bawahan bahwa atas saya tidak puas serta mereka harus memperbaiki pekerjaan mereka.'),
(10, 'Menurut pendapat saya adalah sama pentingnya bagi bawahan untuk menyukainya seperti halnya bagi mereka untuk bekerja keras.', 'Saya membiarkan bawahan untuk mengerjakan sendiri pekerjaannya walaupun mereka mungkin melakukan banyak kesalahan.'),
(11, 'Saya menaruh perhatian terhadap kehidupan pribadi bawahan oleh karena itu saya merasa bahwa mereka mengharapkan hal itu dari saya', 'Saya berpendapat bahwa bawahan tidak selalu harus mengerti mengapa mereka melakukan, sesuatu selama mereka mengerjakan pekerjaan itu.'),
(12, 'Menurut pendapat saya mendisiplinkan bawahan tidak akan memperbaiki mutu ataupun jumlah pekerjaan mereka dikemudian hari.', 'Bila dihadapkan pada suatu persoalan yang sulit, saya mencoba mencapai suatu penyelesaian yang setidak-tidaknya akan diterima oleh sebagian dari yang lain.'),
(13, 'Menurut pikiran saya beberapa bawahan ada yang merasa tidak senang dan saya berusaha mengatasi hal tersebut.', 'Saya mengatur pekerjaan sendiri dan terserah kepada pimpinan yang lebih tinggi untuk mengembangkan ide-ide baru.'),
(14, 'Saya setuju penambahan tunjangan sosial bagi pimpinan dan staf.', 'Saya menaruh perhatian pada peningkatan pengetahuan bawahan mengenai tugas dan organisasi walaupun hal tersebut tidak diperlukan dalam pekerjaan mereka saat ini.'),
(15, 'Saya membiarkan bawahan untuk mengerjakan sendiri pekerjaannya walaupun mungkin melakukan banyak kesalahan.', 'Saya mengambil keputusan sendiri, tetapi akan mempertimbangkan usul-usul yang baik dari para bawahan jika ia memintanya demi memperbaiki keputusan-keputusan itu.'),
(16, 'Jika salah seorang bawahan tidak termasuk kedalam suatu kelompok, saya akan berusaha keras agar orang-orang yang didalam kelompok itu bertemen dengannya.', 'Jika seorang pegawai tidak dapat menyelesaikan suatu tugas, saya membantunya mencapai penyelesaian.'),
(17, 'Menurut pendapat saya salah satu cara meningkatkan disiplin adalah dengan memberi contoh kepada pegawai lain.', 'Kadang-kadang saya tidak berfikir bahwa perasaan dan sikap diri saya adalah sama penting seperti pekerjaan saya.'),
(18, 'Saya tidak menyukai pembicaraan yang tidak perlu diantara bawahan selama mereka bekerja.', 'Saya setuju penambahan tunjangan sosial bagi pimpinan dan staf.'),
(19, 'Saya selalu memperhatikan mereka yang terlambat dan absen.', 'Saya berpendapat bahwa organisasi pegawai akan mencoba melemahkan kekuasaan pimpinan.'),
(20, 'Adakalanya saya tak menyutujui keluhan pegawai sebagai suatu prinsip.', 'Jika keluhan tidak dapat dielakkan (terpaksa timbul), saya akan mengusahakan agar keluhan itu keluar pada tempatnya dan tertib.'),
(21, 'Penting bagi saya untuk mendapat pujian atas pendapat-pendapat saya yang baik.', 'Saya mengeluarkan pendapat-pendapat saya secara terus terang hanya bila saya merasa bahwa yang lain akan sependapat dengan saya.'),
(22, 'Saya berpendapat bahwa organisasi pegawai akan mencoba melemahkan kekuasaan pimpinan.', 'Saya yakin bahwa pertemuan-pertemuan yang seringkali dilakukan dengan perseorangan akan membantu dalam kemajuan mereka.'),
(23, 'Saya berpendapat bahwa bawahan saya tidak selalu harus mengerti mengapa mereka melakukan sesuatu selama mereka mengerjakan pekerjaan itu.', 'Saya berpendapat bahwa alat pencatat waktu absensi mengurangi keterlambatan.'),
(24, 'Saya selalu mengambil keputusan-keputusan sendiri, kemudian memberhentikan bawahan mengenai keputusan-keputusan itu.', 'Saya berpendapat bahwa organisasi pegawai dan pimpinan bekerja untuk tujuan-tujuan yang sama.'),
(25, 'Saya menyetujui pengunaan rencana-rencana rangsangan pembayaran perseorangan.', 'Saya acapkali membiarkan sesuatu diskusi mengarah ke hal-hal yang lain.'),
(26, 'Saya bangga bahwa saya jarang meminta seseorang untuk melakukan pekerjaan yang saya sendiri tidak melakukannya.', 'Menurut pikiran saya beberapa bawahan ada yang merasa tidak senang dan saya berusaha mengatasi hal tersebut.'),
(27, 'Jika suatu pekerjaan begitu penting untuk selesai secepatnya, saya mungkin akan mengerjakannya dengan menyuruh seseorang untuk mengerjakan pekerjaan itu walaupun diperlukan tambahan alat pengaman.', 'Penting bagi saya untuk mendapat pujian atas pendapat-pendapat saya yang baik.'),
(28, 'Tujuannya adalah agar pekerjaan selesai dengan tanpa terlalu banyak pertentangan atau perbedaan pendapat yang tidak perlu.', 'Saya mungkin memberi tugas tanpa terlalu memperhatikan pengalaman atau kemampuan namun menekankan hasilnya.'),
(29, 'Saya mungkin memberi tugas tanpa meperhatikan pengalaman atau kemampuan namun menekankan hasilnya.', 'Dengan sabar saya mendengarkan keluhan-keluhan dan pengaduan-pengaduan tetapi jarang melakukan perbaikan.'),
(30, 'Jika keluhan tidak dapat dielakkan (terpaksa timbul), saya akan mengusahakannya agar keluhan itu keluar pada tempatnya dan tertib.', 'Saya merasa yakin bahwa bawahan-bawahan akan melakukan pekerjaan dengan baik tanpa tekanan saya.'),
(31, 'Bila menghadapi suatu pekerjaan sulit, saya mencoba mencapai suatu penyelesaian yang setidak-tidaknya akan diterima sebagian besar oleh pihak yang berkepentingan.', 'Saya berpendapat bahwa latihan yang didapat dari pengalaman bekerja akan lebih bermanfaat dari pada pendidikan teori.'),
(32, 'Saya selalu menyerahkan pekerjaan-pekerjaan yang paling sulit pada bawahan yang paling berpengalaman.', 'Menurut pendapat saya kenaikan pangkat hanya dilakukan sesuai dengan kemampuan.'),
(33, 'Menurut pendapat saya persoalan-persoalan diantara pegawai akan dapat diselesaikan sendiri oleh mereka tanpa turut campur dari saya.', 'Kalau saya ditegur oleh atasan, saya memanggil bawahan dan saya menyampaikan teguran itu pada mereka.'),
(34, 'Saya tidak memperdulikan apa yang dikerjakan oleh pegawai saya diluar jam kerja.', 'Menurut pendapat saya mendisiplinkan bawahan tidak akan memperbaiki mutu atau jumlah pekerjaan mereka di kemudian hari.'),
(35, 'Saya tidak akan memberikan keterangan kepada pimpinan yang lebih tinggi selain keterangan yang diminta oleh pimpinan itu.', 'Adakalanya saya tidak menyetujui keluhan bawahan sebagai suatu prinsip.'),
(36, 'Kadang-kadang saya merasa ragu untuk mengambil keputusan yang tidak di senangi oleh bawahan.', 'Tujuannya adalah agari pekerjaan selesai dengan tanpa terlalu banyak pertentangan-pertentangan pendapat yang tidak perlu.'),
(37, 'Dengan sabar saya mendengarkan keluhan-keluhan dan pengaduan-pengaduan tetapi jarang melakukan perbaikan.', 'Kadang-kadang saya merasa ragu untuk mengambil keputusan yang tidak di senangi oleh bawahan.'),
(38, 'Saya mengeluarkan pendapat-pendapat saya secara terus terang hanya bila saya merasa bahwa yang lain akan sependapat dengan saya.', 'Kebanyakan dari bawahan saya dapat melakukan pekerjaan tanpa saya apabila perlu.'),
(39, 'Saya mengatur pekerjaan sendiri dan terserah pada pimpinan yang lebih tinggi untuk mengembangkan ide-ide baru.', 'Jika saya memberi tugas, saya menentukan suatu batas waktu untuk melaksanakannya.'),
(40, 'Saya mendorong bawahanan-bawahan untuk mengajukan usul-usul tetapi jarang mengambil tindakan dari usul-usul tersebut.', 'Saya mencoba membuat para pegawai tidak merasa tegang (santai) apabila saya berbicara dengan mereka.'),
(41, 'Dalam diskusi, saya mengemukakan kenyataan yang saya lihat serta terserah yang lain untuk membuat kesimpulan mereka sendiri.', 'Jika atasan memberi suatu instruksi yang tidak mengenakan, saya berpendapat bahwa selayaknya bila instruksi yang tidak menyenangkan itu atas nama atasan saya dan bukan atas nama saya.'),
(42, 'Jika ada pekerjaan yang tidak disenangi yang harus dikerjakan, saya meminta dulu sukarelawan sebelum menugaskan.', 'Saya menaruh perhatian terhadap kehidupan pribadi bawahan, oleh karena saya merasa bahwa mereka mengharapkan hal itu dari saya.'),
(43, 'Saya menaruh perhatian dalam membuat pegawai senang maupun dalam mendorong mereka agar melakukan pekerjaannya.', 'Saya selalu memperhatikan mereka yang terlambat dan absen.'),
(44, 'KEbanyakan dari bawahan saya dapat melakukan pekerjaan tanpa saya apabila perlu.', 'Jika suatu pekerjaan begitu penting untuk selsai secapatnya, saya mungkin akan mengerjakannya dengan menyuruh sesorang untuk mengerjakan pekerjaan walaupun di perlukan tambahan alat pengaman.'),
(45, 'Saya merasa yakin bahwa bawahan saya akan melakukan pekerjaan dengan baik, tanpa tekanan saya.', 'Saya tidak akan memberikan keterangan pada pimpinan yang lebih tinggi selain keterangan yang diminta oleh pimpinan itu.'),
(46, 'Saya yakin bahwa pertemuan-pertemuan yang sering kali dilakukan dengan perseorangan akan membantu dalam kemajuan mereka.', 'Saya menaruh perhatian untuk membuat pekerjaan senang dan dalam mendorong mereka untuk melakukan pekerjaanya.'),
(47, 'Saya menaruh perhatian pada peningkatan pengetahuan bawahan mengenai tugas dan departemen walaupun hal tersebut tidak diperlukan dalam pekerjaan mereka saat ini.', 'Saya mengawasi secara dekat pegawai-pegawai yang terbelakang dalam pekerjaan atau bekerja secara tidak memuaskan.'),
(48, 'Saya mengizinkan bawahan untuk turut serta dalam membuat keputusan dan selalu berpegang pada keputusan mayoritas', 'Saya membuat bawahan kerja keras tetapi berusaha memastikan bahwa mereka selalu mendapat imbalan yang adil dari pimpinan yang lebih tinggi.'),
(49, 'Ia berpendapat bahwa semua pegawai yang melakukan pekerjaan serupa seharusnya menerima imbalan yang sama.', 'Bila pekerjaan seorang pegawai terus-menerus tidak memuaskan, saya akan menunggu sampai ada kesempatan untuk memutasikan pegawai itu daripada memecatnya.'),
(50, 'Saya berpendapat bahwa tujuan bawahan dan pimpinan berlawanan tetapi saya berusaha tidak mengeluarkan pendapat saya secara terus terang.', 'Menurut pendapat saya, sama penting bagi bawahan untuk menyukainya seperti halnya bagi mereka untuk bekerja keras.'),
(51, 'Saya mengawasi secara dekat pegawai-pegawai yang terbelakang dalam pekerjaan atau bekerja secara tidak memuaskan.', 'Saya tidak menyetujui pembicaraan yang tidak perlu diantara bawahan selama mereka bekerja.'),
(52, 'Jika saya memberi tugas, saya menentukan suatu batas waktu untuk melaksanakannya.', 'Saya bangga bahwa saya jarang meminta seseorang untuk melakukan pekerjaan yang saya sendiri tidak akan melakukannya.'),
(53, 'Saya berpendapat bahwa latihan yang didapat dari pengalaman selama bekerja lebih bermanfaat daripada pendidikan teori.', 'Saya tidak memperdulikan apa yang di kerjakan pegawai saya diluar jam kerja.'),
(54, 'Saya berpendapat bahwa alat pencatat waktu absensi mengurangi keterlambatan pekerja.', 'Saya mengijinkan bawahan saya untuk turut serta dalam membuat keputusan dan selalu berpegang pada keputusan mayoritas.'),
(55, 'Saya mengambil keputusan sendiri, tetapi akan mempertimbangkan usul-usul yang baik dari para bawahan jika ia memintanya demi memperbaiki keputusan-keputusan itu.', 'Saya berpendapat bahwa tujuan bawahan dan pimpinan berlawanan tetapi saya berusaha untuk tidak mengeluarkan pendapat saya secara terus terang.'),
(56, 'Saya mengambil keputusan sendiri dan kemudian mencoba \"menyampaikan\" nya kepada bawahan.', 'Kalau kemungkinan, saya ingin membentuk tim kerja yang terdiri dari orang-orang yang telah berteman baik satu dengan yang lainnya.'),
(57, 'Saya tidak akan ragu-ragu untuk menerima pegawai cacat apabila saya merasa orang tersebut sanggup belajar mengajar pekerjaan itu.', 'Saya tidak memperdulikan pelanggaran atas peraturan apabila saya yakin bahwa tidak ada orang lain yang mengetahui pelanggaran itu.'),
(58, 'Kalau memungkinkan, saya akan membentuk Tim Kerja yang terdiri dari orang-orang yang telah berteman baik satu sama lainnya.', 'Saya mungkin menyerahkan tugas yang sulit kepada bawahan yang tidak mempunyai pengalaman, tetapi jika mereka mendapatkan kesulitan dalam mengerjakan kesulitan itu, saya akan mengambil alih pekerjaan itu dan mengerjakannya sendiri atau dikerjakan oleh orang lain.'),
(59, 'Saya membuat bawahan saya kerja keras tetapi berusaha memastikan bahwa mereka selalu mendapat imbalan yang adil dari pimpinan yang lebih tinggi.', 'Menurut pendapat saya salah satu cara untuk meningkatkan disiplin adalah dengan memberi contoh kepada pegawai lain.'),
(60, 'Saya mencoba membuat para pegawai saya tidak merasa tegang (santai) apabila saya berbicara dengan mereka.', 'Saya menyetujui penggunaan rencana dengan rangsangan pembayaran-pembayaran perorangan.'),
(61, 'Saya berpendapat bahwa kenaikan pangkat hanya dilakukan sesuai dengan kemampuan.', 'Menurut pendapat saya persoalan-persoalan diantara pegawai akan dapat diselesaikan sendiri oleh mereka tanpa turut campur tangan dari saya.'),
(62, 'Saya berpendapat bahwa organisasi pegawai dan pimpinan bekerjan untuk tujuan-tujuan yang sama.', 'Dalam diskusi, saya mengemukakan kenyataan yang saya lihat serta terserah yang lain untuk membuat kesimpulan mereka sendiri.'),
(63, 'Jika seseorang pegawai tidak dapat menyelesaikan suatu tugas, saya membantunya mencapai penyelesaian.', 'Saya berpendapat bahwa semua pekerja yang melakukan pekerjaan yang serupa seharusnya menerima imbalan yang sama.'),
(64, 'Saya mengizinkan bawahan saya untuk turut serta dalam membuat keputusan tetapi keputusan terakhir tetap berada ditangan saya.', 'Saya tidak akan ragu-ragu untuk menerima pegawai cacat apabila saya merasa bahwa orang tersebut sanggup belajar mengerjakan pekerjaan itu.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jawaban_kecerdasan_1`
--
ALTER TABLE `jawaban_kecerdasan_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jawaban_kepribadian_daya_juang`
--
ALTER TABLE `jawaban_kepribadian_daya_juang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_soal` (`id_soal`);

--
-- Indexes for table `jawaban_kepribadian_sifat`
--
ALTER TABLE `jawaban_kepribadian_sifat`
  ADD PRIMARY KEY (`id_uji`),
  ADD KEY `uji_kepribadian_ibfk_1` (`username`),
  ADD KEY `uji_kepribadian_ibfk_2` (`id_kepribadian`);

--
-- Indexes for table `kepribadian`
--
ALTER TABLE `kepribadian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id_role` (`id_role`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_1`
--
ALTER TABLE `soal_kecerdasan_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_2`
--
ALTER TABLE `soal_kecerdasan_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_3`
--
ALTER TABLE `soal_kecerdasan_3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_4`
--
ALTER TABLE `soal_kecerdasan_4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_5`
--
ALTER TABLE `soal_kecerdasan_5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_6`
--
ALTER TABLE `soal_kecerdasan_6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_7`
--
ALTER TABLE `soal_kecerdasan_7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kecerdasan_8`
--
ALTER TABLE `soal_kecerdasan_8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kepribadian_1`
--
ALTER TABLE `soal_kepribadian_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kepribadian_2`
--
ALTER TABLE `soal_kepribadian_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kepribadian_daya_juang`
--
ALTER TABLE `soal_kepribadian_daya_juang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kepribadian_gaya_kepemimpinan`
--
ALTER TABLE `soal_kepribadian_gaya_kepemimpinan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_kepribadian_gaya_manajemen`
--
ALTER TABLE `soal_kepribadian_gaya_manajemen`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jawaban_kepribadian_sifat`
--
ALTER TABLE `jawaban_kepribadian_sifat`
  MODIFY `id_uji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kepribadian`
--
ALTER TABLE `kepribadian`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jawaban_kepribadian_daya_juang`
--
ALTER TABLE `jawaban_kepribadian_daya_juang`
  ADD CONSTRAINT `jawaban_kepribadian_daya_juang_ibfk_1` FOREIGN KEY (`id_soal`) REFERENCES `soal_kepribadian_daya_juang` (`id`);

--
-- Constraints for table `jawaban_kepribadian_sifat`
--
ALTER TABLE `jawaban_kepribadian_sifat`
  ADD CONSTRAINT `jawaban_kepribadian_sifat_ibfk_1` FOREIGN KEY (`username`) REFERENCES `member` (`username`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jawaban_kepribadian_sifat_ibfk_2` FOREIGN KEY (`id_kepribadian`) REFERENCES `kepribadian` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
