<?php 

class RekapModel extends CI_Model{

	public function RekapUjiKepribadianSifat()
	{
		$this->db->select('nama, tempat_lahir, tanggal_lahir, jawaban_positif, jawaban_negatif, kepribadian');
		$this->db->from('member');
		$this->db->join('jawaban_kepribadian_sifat', 'jawaban_kepribadian_sifat.username = member.username');
		$this->db->join('kepribadian','jawaban_kepribadian_sifat.id_kepribadian = kepribadian.id');

		return $this->db->get();
	}
}