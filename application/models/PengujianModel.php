<?php 

class PengujianModel extends CI_Model{

	public function GetSoalKepribadian($key)
	{
		if($key == 1)
			$this->db->from("soal_kepribadian_1");
		else
			$this->db->from("soal_kepribadian_2");
		$this->db->order_by("RAND()");
		return $this->db->get();
	}

	public function GetSoalKepribadianDayaJuang()
	{
		$this->db->from('soal_kepribadian_daya_juang');
		$this->db->join('jawaban_kepribadian_daya_juang', 'soal_kepribadian_daya_juang.id = jawaban_kepribadian_daya_juang.id_soal');
		return $this->db->get();
	}

	public function GetSoalKepribadianGayaKepemimpinan()
	{
		$this->db->from("soal_kepribadian_gaya_kepemimpinan");
		$this->db->order_by("RAND()");
		return $this->db->get();
	}

	public function GetSoalKepribadianGayaManajemen()
	{
		$this->db->from("soal_kepribadian_gaya_manajemen");
		$this->db->order_by("RAND()");
		return $this->db->get();
	}

	public function GetSoalKecerdasan()
	{
		$this->db->from("soal_kecerdasan_1");
		$this->db->order_by("RAND()");
		return $this->db->get();
	}

	public function InsertUjiKepribadian($data){
		$this->db->insert("jawaban_kepribadian_sifat",$data);
	}

	public function UpdateUjiKepribadian($username,$data){
		$this->db->where('username',$username);
		$this->db->replace('jawaban_kepribadian_sifat',$data);
	}

	public function GetKepribadian($jumlah_a,$jumlah_b,$jumlah_c,$jumlah_d){
		$kepribadian = 1;
		$total = 0;
		if($total < $jumlah_a){
			$total = $jumlah_a;
			$kepribadian = 1;
		}

		if($total < $jumlah_b){
			$total = $jumlah_b;
			$kepribadian = 2;
		}

		if($total < $jumlah_c){
			$total = $jumlah_c;
			$kepribadian = 3;
		}

		if($total < $jumlah_d){
			$total = $jumlah_d;
			$kepribadian = 4;
		}

		return $kepribadian;
	}

	public function UserKepribadian($username){
		$this->db->from('jawaban_kepribadian_sifat');
		$this->db->where('username',$username);
		$this->db->where('jawaban_positif !=', '');
		$this->db->where('jawaban_negatif !=', '');
		$result = $this->db->get()->row();

		return isset($result);
	}

	public function GetUjiKepribadianSifat()
	{
		$this->db->select('kepribadian, COUNT(id_kepribadian) as jumlah');
		$this->db->from('jawaban_kepribadian_sifat');
		$this->db->join('kepribadian','jawaban_kepribadian_sifat.id_kepribadian = kepribadian.id','right');
		$this->db->group_by('kepribadian');
		return $this->db->get()->result();
	}
}