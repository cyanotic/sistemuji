<?php 

class UserModel extends CI_Model{

	public function GetUserData($username)
	{
		return $this->db->get_where('member',array("username" => $username));
	}

	public function CheckLogin($user,$password){
		$result = $this->db->get_where('member',array("email" => $user, "passHashed" => md5($password)))->row();
		if(!isset($result))
			$result = $this->db->get_where('member',array("username" => $user, "passHashed" => md5($password)))->row();
		return $result;
	}
}