<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->view('shared/styles')?>
  <title>Sistem Pengujian | Dashboard Admin</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
          <small>Index</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Home"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="ac">Index</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-md-6 col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Kepribadian Sifat</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <?php
                foreach($grafik as $item){
                  $kepribadian[] = $item->kepribadian;
                  $jumlah[] = (float) $item->jumlah;
                }
                ?>
                <div class="chart">
                  <canvas id="canvas" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>

    <!-- Footer -->
    <?php $this->view('shared/footer')?>
    <!-- End Of Footer -->
  </div>
  <?php $this->view('shared/script')?>
  <script type="text/javascript">
    var lineChartData = {
      labels : <?php echo json_encode($kepribadian);?>,
      datasets: [{
        fillColor: "rgba(60,141,188,0.9)",
        strokeColor: "rgba(60,141,188,0.8)",
        pointColor: "#3b8bba",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(152,235,239,1)",
        data : <?php echo json_encode($jumlah);?>
      }]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    var barChartDemo = new Chart(ctx).Line(lineChartData, {
      responsive: true
    });
  </script>
</body>
</html>