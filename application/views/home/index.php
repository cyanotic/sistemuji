<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->view('shared/styles')?>
  <title>Sistem Pengujian | Pengujian Kepribadian</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pengujian Kepribadian
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Pengujian"><i class="fa fa-table"></i> Pengujian</a></li>
          <li class="ac">Kepribadian</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
          <div class="callout callout-info">
            <h4>Tip!</h4>

            <p>Add the fixed class to the body tag to get this layout. The fixed layout is your best option if your sidebar
            is bigger than your content because it prevents extra unwanted scrolling.</p>
          </div>
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3>Data Diri</h3>
            </div>
            <div class="box-body">
              <h4>Soal Kepribadian</h4>
              <p>1. Sebelum mulai mengerjakan soal, diharapkan menggunakan koneksi internet yang
              stabil.</p>
              <p>2. Bacalah dengan cermat aturan dan tata cara menjawab setiap soal.</p>
              <p>3. Pilihlah jawaban yang sesuai dengan kepribadian anda.</p>
              <p>4. Waktu yang disediakan untuk pengerjaan soal adalah 5 menit.</p>
            </div>
            <div class="box-body">
              <h4>Soal Kecerdasan</h4>
              <p>1. Sebelum mulai mengerjakan soal, diharapkan menggunakan koneksi internet yang
              stabil.</p>
              <p>2. Bacalah dengan cermat aturan dan tata cara menjawab setiap soal.</p>
              <p>3. Pilihlah jawaban yang dengan cepat dan tepat.</p>
              <p>4. Waktu yang disediakan untuk pengerjaan soal adalah 60 menit.</p>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->
      </section>
    </div>

    <!-- Footer -->
    <?php $this->view('shared/footer')?>
    <!-- End Of Footer -->
  </div>
  <?php $this->view('shared/script')?>
</body>
</html>