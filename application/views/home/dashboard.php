<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('shared/styles')?>
	<title>Welcome Page</title>
</head>
<body>
<div class="wrapper">
	<?php $this->load->view('shared/sidebar')?>
	<div class="main-panel ps-container ps-theme-default" data-ps-id="f9a32de5-ecc2-5eae-98f1-437ab5bc1666">
      <!-- Navbar -->
      <?php $this->load->view('shared/navbar')?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Lorem Ipsum</h4>
              <p class="card-category">Petunjuk Pengujian</p>
            </div>
            <div class="card-body">
              <div id="typography">
                <div class="card-title">
                  <h2>Petunjuk Pengujian</h2>
                </div>
                <div class="row">
                  <div class="tim-typo">
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit Dolor Amet</h3>
                    <h3>1. Lorem Ipsum Sit D</h3>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <?php $this->load->view('shared/footer')?>
      <!-- End of footer -->
    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
</div>
<?php $this->load->view('shared/script')?>
</body>
</html>