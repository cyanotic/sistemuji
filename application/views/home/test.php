<!DOCTYPE html>
<html class="perfect-scrollbar-on" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<link rel="apple-touch-icon" sizes="76x76" href="https://demos.creative-tim.com/material-dashboard-pro/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="https://demos.creative-tim.com/material-dashboard-pro/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Material Dashboard PRO  by Creative Tim</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport">
	<link rel="canonical" href="https://www.creative-tim.com/product/material-dashboard-pro">
	<meta name="keywords" content="creative tim, html dashboard, html css dashboard, web dashboard, bootstrap 4 dashboard, bootstrap 4, css3 dashboard, bootstrap 4 admin, material dashboard bootstrap 4 dashboard, frontend, responsive bootstrap 4 dashboard, material design, material dashboard bootstrap 4 dashboard">
	<meta name="description" content="Material Dashboard PRO is a Premium Material Bootstrap 4 Admin with a fresh, new design inspired by Google's Material Design.">
	<meta itemprop="name" content="Material Dashboard PRO by Creative Tim">
	<meta itemprop="description" content="Material Dashboard PRO is a Premium Material Bootstrap 4 Admin with a fresh, new design inspired by Google's Material Design.">
	<meta itemprop="image" content="https://s3.amazonaws.com/creativetim_bucket/products/51/original/opt_mdp_thumbnail.jpg">
	<meta name="twitter:card" content="product">
	<meta name="twitter:site" content="@creativetim">
	<meta name="twitter:title" content="Material Dashboard PRO by Creative Tim">

	<meta name="twitter:description" content="Material Dashboard PRO is a Premium Material Bootstrap 4 Admin with a fresh, new design inspired by Google's Material Design.">
	<meta name="twitter:creator" content="@creativetim">
	<meta name="twitter:image" content="https://s3.amazonaws.com/creativetim_bucket/products/51/original/opt_mdp_thumbnail.jpg">
	<meta property="fb:app_id" content="655968634437471">
	<meta property="og:title" content="Material Dashboard PRO by Creative Tim">
	<meta property="og:type" content="article">
	<meta property="og:url" content="http://demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html">
	<meta property="og:image" content="https://s3.amazonaws.com/creativetim_bucket/products/51/original/opt_mdp_thumbnail.jpg">
	<meta property="og:description" content="Material Dashboard PRO is a Premium Material Bootstrap 4 Admin with a fresh, new design inspired by Google's Material Design.">
	<meta property="og:site_name" content="Creative Tim">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/login/font-awesome.css">
	<link href="<?php echo base_url()?>assets/login/material-dashboard.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/login/demo.css" rel="stylesheet">
	<script src="<?php echo base_url()?>assets/login/111649226022273.js" async=""></script><script async="" src="<?php echo base_url()?>assets/login/fbevents.js"></script><script type="text/javascript" async="" src="<?php echo base_url()?>assets/login/ga.js">
	</script>
	<script async="" src="<?php echo base_url()?>assets/login/gtm.js"></script>
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NKDMSK6');
</script>
<!-- End Google Tag Manager -->
<script type="text/javascript" charset="UTF-8" src="<?php echo base_url()?>assets/login/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo base_url()?>assets/login/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo base_url()?>assets/login/stats.js"></script>
</head>
<body class="off-canvas-sidebar">

	<!-- Extra details for Live View on GitHub Pages -->
	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden">
		</iframe>
	</noscript>
		<!-- End Google Tag Manager (noscript) -->
		<!-- Navbar -->
		<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white" id="navigation-example">
			<div class="container">
				<div class="navbar-wrapper">
					<a class="navbar-brand" href="<?php echo base_url()?>">Sistem Pengujian</a>
				</div>

				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation" data-target="#navigation-example">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>

				<div class="collapse navbar-collapse justify-content-end">


					<ul class="navbar-nav">
						<!-- <li class="nav-item">
							<a href="https://demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html" class="nav-link">
								<i class="material-icons">dashboard</i>
								Dashboard
							</a>
						</li>
						<li class="nav-item ">
							<a href="https://demos.creative-tim.com/material-dashboard-pro/examples/pages/register.html" class="nav-link">
								<i class="material-icons">person_add</i>
								Register
							</a>
						</li>
						<li class="nav-item  active ">
							<a href="https://demos.creative-tim.com/material-dashboard-pro/examples/pages/login.html" class="nav-link">
								<i class="material-icons">fingerprint</i>
								Login
							</a>
						</li> -->

						<!-- <li class="nav-item ">
							<a href="https://demos.creative-tim.com/material-dashboard-pro/examples/pages/lock.html" class="nav-link">
								<i class="material-icons">lock_open</i>
								Lock
							</a>
						</li> -->
					</ul>
				</div>
			</div>
		</nav>
		<!-- End Navbar -->
		<div class="wrapper wrapper-full-page">
			<div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../../assets/img/login.jpg'); background-size: cover; background-position: top center;">
				<!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
				<div class="container">
					<div class="col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto">
						<form class="form" method="" action="">
							<div class="card card-login">
								<div class="card-header card-header-rose text-center">
									<h4 class="card-title">Login</h4>
								</div>
								<div class="card-body ">
									<p class="card-description text-center">Or Be Classical</p>
									<span class="bmd-form-group">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">
													<i class="material-icons">person</i>
												</span>
											</div>
											<input class="form-control" placeholder="Username..." type="text">
										</div>
									</span>
									<span class="bmd-form-group">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">
													<i class="material-icons">lock_outline</i>
												</span>
											</div>
											<input class="form-control" placeholder="Password..." type="password">
										</div>
									</span>
								</div>
								<div class="card-footer justify-content-center">
									<input type="submit" name="submit" value="Lets Go" class="btn btn-rose btn-link btn-lg">
								</div>
							</div>
						</form>
					</div>
				</div>
				<footer class="footer">
					<div class="container">
						<nav class="float-left">
							<ul>
								<li>
									<a href="https://www.creative-tim.com/">
										Creative Tim
									</a>
								</li>
								<li>
									<a href="https://creative-tim.com/presentation">
										About Us
									</a>
								</li>
								<li>
									<a href="http://blog.creative-tim.com/">
										Blog
									</a>
								</li>
								<li>
									<a href="https://www.creative-tim.com/license">
										Licenses
									</a>
								</li>
							</ul>
						</nav>
						<div class="copyright float-right">
							©
							<script>
								document.write(new Date().getFullYear())
							</script>2018, made with <i class="material-icons">favorite</i> by
							<a href="https://www.creative-tim.com/" target="_blank">Creative Tim</a> for a better web.
						</div>
					</div>
				</footer>

			</div>


		</div>

		<!--   Core JS Files   -->
		<script src="<?php echo base_url()?>assets/login/jquery_004.js" type="text/javascript"></script>
		<script src="<?php echo base_url()?>assets/login/popper.js" type="text/javascript"></script>
		<script src="<?php echo base_url()?>assets/login/bootstrap-material-design.js" type="text/javascript"></script>

		<script src="<?php echo base_url()?>assets/login/perfect-scrollbar.js"></script>
		<!-- Plugin for the momentJs  -->
		<script src="<?php echo base_url()?>assets/login/moment.js"></script>
		<!--  Plugin for Sweet Alert -->
		<script src="<?php echo base_url()?>assets/login/sweetalert2.js"></script>
		<!-- Forms Validations Plugin -->
		<script src="<?php echo base_url()?>assets/login/jquery_005.js"></script>
		<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
		<script src="<?php echo base_url()?>assets/login/jquery_003.js"></script>
		<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
		<script src="<?php echo base_url()?>assets/login/bootstrap-selectpicker.js"></script>
		<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
		<script src="<?php echo base_url()?>assets/login/bootstrap-datetimepicker.js"></script>
		<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
		<script src="<?php echo base_url()?>assets/login/jquery.js"></script>
		<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
		<script src="<?php echo base_url()?>assets/login/bootstrap-tagsinput.js"></script>
		<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
		<script src="<?php echo base_url()?>assets/login/jasny-bootstrap.js"></script>
		<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
		<script src="<?php echo base_url()?>assets/login/fullcalendar.js"></script>
		<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
		<script src="<?php echo base_url()?>assets/login/jquery-jvectormap.js"></script>
		<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
		<script src="<?php echo base_url()?>assets/login/nouislider.js"></script>
		<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
		<script src="<?php echo base_url()?>assets/login/core.js"></script>
		<!-- Library for adding dinamically elements -->
		<script src="<?php echo base_url()?>assets/login/arrive.js"></script>
		<!--  Google Maps Plugin    -->
		<script src="<?php echo base_url()?>assets/login/js"></script>
		<!-- Place this tag in your head or just before your close body tag. -->
		<script async="" defer="defer" src="<?php echo base_url()?>assets/login/buttons.js"></script>
		<!-- Chartist JS -->
		<script src="<?php echo base_url()?>assets/login/chartist.js"></script>
		<!--  Notifications Plugin    -->
		<script src="<?php echo base_url()?>assets/login/bootstrap-notify.js"></script>
		<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc --><script src="<?php echo base_url()?>assets/login/material-dashboard.js" type="text/javascript"></script>
		<!-- Material Dashboard DEMO methods, don't include it in your project! -->
		<script src="<?php echo base_url()?>assets/login/demo.js"></script>
		<script>
			$(document).ready(function(){
				$().ready(function(){
					$sidebar = $('.sidebar');

					$sidebar_img_container = $sidebar.find('.sidebar-background');

					$full_page = $('.full-page');

					$sidebar_responsive = $('body > .navbar-collapse');

					window_width = $(window).width();

					fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

					if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
						if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
							$('.fixed-plugin .dropdown').addClass('open');
						}

					}

					$('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
          	if (event.stopPropagation) {
          		event.stopPropagation();
          	} else if (window.event) {
          		window.event.cancelBubble = true;
          	}
          }
      });

					$('.fixed-plugin .active-color span').click(function() {
						$full_page_background = $('.full-page-background');

						$(this).siblings().removeClass('active');
						$(this).addClass('active');

						var new_color = $(this).data('color');

						if ($sidebar.length != 0) {
							$sidebar.attr('data-color', new_color);
						}

						if ($full_page.length != 0) {
							$full_page.attr('filter-color', new_color);
						}

						if ($sidebar_responsive.length != 0) {
							$sidebar_responsive.attr('data-color', new_color);
						}
					});

					$('.fixed-plugin .background-color .badge').click(function() {
						$(this).siblings().removeClass('active');
						$(this).addClass('active');

						var new_color = $(this).data('background-color');

						if ($sidebar.length != 0) {
							$sidebar.attr('data-background-color', new_color);
						}
					});

					$('.fixed-plugin .img-holder').click(function() {
						$full_page_background = $('.full-page-background');

						$(this).parent('li').siblings().removeClass('active');
						$(this).parent('li').addClass('active');


						var new_image = $(this).find("img").attr('src');

						if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
							$sidebar_img_container.fadeOut('fast', function() {
								$sidebar_img_container.css('background-image', 'url("' + new_image + '")');
								$sidebar_img_container.fadeIn('fast');
							});
						}

						if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
							var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

							$full_page_background.fadeOut('fast', function() {
								$full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
								$full_page_background.fadeIn('fast');
							});
						}

						if ($('.switch-sidebar-image input:checked').length == 0) {
							var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
							var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

							$sidebar_img_container.css('background-image', 'url("' + new_image + '")');
							$full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
						}

						if ($sidebar_responsive.length != 0) {
							$sidebar_responsive.css('background-image', 'url("' + new_image + '")');
						}
					});

					$('.switch-sidebar-image input').change(function() {
						$full_page_background = $('.full-page-background');

						$input = $(this);

						if ($input.is(':checked')) {
							if ($sidebar_img_container.length != 0) {
								$sidebar_img_container.fadeIn('fast');
								$sidebar.attr('data-image', '#');
							}

							if ($full_page_background.length != 0) {
								$full_page_background.fadeIn('fast');
								$full_page.attr('data-image', '#');
							}

							background_image = true;
						} else {
							if ($sidebar_img_container.length != 0) {
								$sidebar.removeAttr('data-image');
								$sidebar_img_container.fadeOut('fast');
							}

							if ($full_page_background.length != 0) {
								$full_page.removeAttr('data-image', '#');
								$full_page_background.fadeOut('fast');
							}

							background_image = false;
						}
					});

					$('.switch-sidebar-mini input').change(function() {
						$body = $('body');

						$input = $(this);

						if (md.misc.sidebar_mini_active == true) {
							$('body').removeClass('sidebar-mini');
							md.misc.sidebar_mini_active = false;

							$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

						} else {

							$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

							setTimeout(function() {
								$('body').addClass('sidebar-mini');

								md.misc.sidebar_mini_active = true;
							}, 300);
						}

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
          	window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
          	clearInterval(simulateWindowResize);
          }, 1000);

      });
				});
});
</script>

<!-- Sharrre libray -->
<script src="<?php echo base_url()?>assets/login/jquery_002.js"></script>

<noscript>
	<img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1"
	/>

</noscript>
<script>
	$(document).ready(function(){
		demo.checkFullPageBackgroundImage();setTimeout(function(){
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
    }, 700);});
</script>
</body>
</html>