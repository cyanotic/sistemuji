<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$username = $this->session->username;
$user = $this->db->query("SELECT * FROM member WHERE username = '$username'")->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('shared/styles')?>
	<title>Sistem Pengujian | Blank</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div clheadass="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Blank
          <small>Index</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Home"><i class="fa fa-dashboard"></i> Blank</a></li>
          <li class="ac">Index</li>
        </ol>
      </section>
      <section class="content">
        
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Soal</h3>
          </div>
          <form role="form">
            <div class="box-body">
              <div class="col-md-1"></div>
              <div class="col-md-10">
                <div class="form-group">
                  <label>Ini soal nomor 1, berapakah</label>
                  <!-- <input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email"> -->
                </div>
                <div class="form-group">
                  <label>
                    <input type="radio" name="r3" class="flat-red" value="1"> 1 + 1 = 2
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="radio" name="r3" class="flat-red" value="2"> 2 + 2 = 5
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="radio" name="r3" class="flat-red" value="3"> 2 + 4 = 10
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="radio" name="r3" class="flat-red" value="4"> Pilihan 4
                  </label>
                </div><!-- 
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input id="exampleInputFile" type="file">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
 --><!--                 <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>
     -->          </div>
              <div class="col-md-1"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="col-md-7"></div>
              <div class="col-md-5">
                <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                  <ul class="pagination">
                    <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                    <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                    <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                    <li></li>
                    <li><button type="submit" class="btn btn-primary">Finish</button></li>
                  </ul>
                </div>
              </div>
            </div>
          </form>
        </div>

      </section>
      <!-- /.content -->
    </div>

    <!-- Footer -->
    <?php $this->view('shared/footer')?>
    <!-- End Of Footer -->
    <!-- /.control-sidebar -->
  </div>
  <?php $this->view('shared/script')?>
</body>
</html>