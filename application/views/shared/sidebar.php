<?php
$username = $this->session->username;
$user = $this->db->get_where('member',array("username" => $username))->row();
$controller = $this->uri->segment(1);
$method = $this->uri->segment(2);
$id = $this->uri->segment(3);
if($controller == "")
  $controller = "Home";
if($method == "")
  $method = "Index";
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url()?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $user->nama?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li <?php if($controller == "Home") echo "class='active'"?>><a href="<?php echo base_url()?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <?php if($user->id_role == 2):?>
        <li class="treeview <?php if($controller == "Pengujian") echo "active"?>">
          <a href="#">
            <i class="fa fa-check"></i> <span>Pengujian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview <?php if($controller == "Pengujian" && $method == "Kepribadian") echo "active"?>"><a href="#"><i class="fa fa-circle-o"></i> Kepribadian
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li <?php if($controller == "Pengujian" && $method == "KepribadianSifat") echo "class='active'"?>><a href="<?php echo base_url()?>Pengujian/KepribadianSifat"><i class="fa fa-circle-o"></i> Sifat</a></li>
              <li <?php if($controller == "Pengujian" && $method == "KepribadianDayaJuang") echo "class='active'"?>><a href="<?php echo base_url()?>Pengujian/KepribadianDayaJuang"><i class="fa fa-circle-o"></i> Daya Juang</a></li>
              <li <?php if($controller == "Pengujian" && $method == "KepribadianGayaKepemimpinan") echo "class='active'"?>><a href="<?php echo base_url()?>Pengujian/KepribadianGayaKepemimpinan"><i class="fa fa-circle-o"></i> Gaya Kepemimpinan</a></li>
              <li <?php if($controller == "Pengujian" && $method == "KepribadianGayaManajemen") echo "class='active'"?>><a href="<?php echo base_url()?>Pengujian/KepribadianGayaManajemen"><i class="fa fa-circle-o"></i> Gaya Manajemen</a></li>
            </ul>
          </li>


          <li <?php if($controller == "Pengujian" && $method == "Kecerdasan") echo "class='active'"?>><a href="<?php echo base_url()?>Pengujian/Kecerdasan"><i class="fa fa-circle-o"></i> Kecerdasan</a></li>
        </ul>
      </li>
    <?php endif;?>
    <?php if($user->id_role == 1):?>
      <li class="treeview <?php if($controller == "Rekap") echo "active"?>">
        <a href="#">
          <i class="fa fa-table"></i> <span>Rekap Hasil</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview <?php if($controller == "Rekap" && $method == "Kepribadian") echo " active"?>">
            <a href="#"><i class="fa fa-circle-o"></i> Kepribadian
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li <?php if($controller == "Rekap" && $method == "Kepribadian" && (strtolower($id) == "sifat" || $id == "")) echo "class='active'"?>><a href="<?php echo base_url()?>Rekap/Kepribadian/Sifat"><i class="fa fa-circle-o"></i> Sifat</a></li>
              <li <?php if($controller == "Rekap" && $method == "Kepribadian" && (strtolower($id) == "daya_juang" || $id == "")) echo "class='active'"?>><a href="<?php echo base_url()?>Rekap/Kepribadian/DayaJuang"><i class="fa fa-circle-o"></i> Daya Juang</a></li>
              <li <?php if($controller == "Rekap" && $method == "Kepribadian" && (strtolower($id) == "gaya_kepemimpinan" || $id == "")) echo "class='active'"?>><a href="<?php echo base_url()?>Rekap/Kepribadian/GayaKepemimpinan"><i class="fa fa-circle-o"></i> Gaya Kepemimpinan</a></li>
              <li <?php if($controller == "Rekap" && $method == "Kepribadian" && (strtolower($id) == "gaya_manajemen" || $id == "")) echo "class='active'"?>><a href="<?php echo base_url()?>Rekap/Kepribadian/GayaManajemen"><i class="fa fa-circle-o"></i> Gaya Manajemen</a></li>

            </ul>
          </li>
          <li><a href="<?php echo base_url()?>Rekap/Kecerdasan"><i class="fa fa-circle-o"></i> Kecerdasan</a></li>
        </ul>
      </li>
      <li <?php if($controller == "Member") echo "class='active'"?>><a href="<?php echo base_url()?>Member"><i class="glyphicon glyphicon-user"></i><span> Member</span></a>
      </li>
    <?php endif;?>
  </ul>
</section>
<!-- /.sidebar -->
</aside>