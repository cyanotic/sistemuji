<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->view('shared/styles')?>
  <title>Sistem Pengujian | Daftar Peserta</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Peserta
          <small>Tambah</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Member"><i class="glyphicon glyphicon-user"></i> Member</a></li>
          <li class="ac">Tambah Peserta</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Tambah Peserta </h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form method="post" action="<?php echo base_url()?>Member/TambahPost" class="form-horizontal" id="form-tambah">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="username" readonly="readonly" class="form-control col-md-7 col-xs-12" id="username" required="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="password" readonly="readonly" class="form-control col-md-7 col-xs-12" id="password" required="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="nama" class="form-control col-md-7 col-xs-12" required="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="email" name="email" class="form-control col-md-7 col-xs-12" required="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="tempat_lahir" class="form-control col-md-7 col-xs-12" required="">
                    </div>
                  </div>

                  <div class="form-group input-append date">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="date" placeholder="yyyy-mm-dd" name="tanggal_lahir" class="form-control col-md-7 col-xs-12" required="" max="<?php echo date('Y-m-d')?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <center>
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="<?php echo base_url()?>Member" class="btn btn-default">Batal</a>
                        <a href="javascript:void(0)" class="btn btn-info" id="generate">Generate Akun</a>
                        <input type="submit" class="btn btn-success" value="Simpan" />
                      </div>
                    </center>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>
    <?php $this->view('shared/footer')?>
    <?php $this->view('shared/script')?>
    <!-- page script -->


    <script>

      function acakPass(panjang)
      {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < panjang; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
      }

      function acakUser(panjang)
      {
        var text = "";
        var possibleHuruf = "abcdefghijklmnopqrstuvwxyz";
        var possibleAngka = "0123456789";

        var huruf = panjang - 3;
        for (var i = 0; i < huruf; i++)
          text += possibleHuruf.charAt(Math.floor(Math.random() * possibleHuruf.length));
        for(var i = 0; i < 3; i++)
          text += possibleAngka.charAt(Math.floor(Math.random() * possibleAngka.length));

        return text;
      }

      $('#generate').click(function(){
        $('#username').val(acakUser(7));
        $('#password').val(acakPass(7));
      });

      $('#form-tambah').submit(function(e){
        if($('#username').val() == null || $('#username').val() == ""){
          e.preventDefault();
          alert("Silakan Klik Generate Akun Untuk Mendapatkan Username Dan Password");
        }
      })

    </script>
  </body>
  </html>