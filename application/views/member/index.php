<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('shared/styles')?>
	<title>Sistem Pengujian | Daftar Peserta</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Daftar Peserta
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Member"><i class="glyphicon glyphicon-user"></i> Member</a></li>
          <li class="ac">Daftar Peserta</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Daftar Peserta </h3>
                <ul class="nav navbar-right col-md-2 col-xs-12">
                  <li><a href="<?php echo base_url()?>Member/Tambah" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>  Tambah Peserta</a></li>
                </ul>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Nama</th>
                      <th>Tempat Lahir</th>
                      <th>Tanggal Lahir</th>
                      <th>E-Mail</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach($member as $m){
                      ?>
                      <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $m->username ?></td>
                        <td><?php echo $m->password ?></td>
                        <td><?php echo $m->nama ?></td>
                        <td><?php echo $m->tempat_lahir ?></td>
                        <td><?php echo $m->tanggal_lahir ?></td>
                        <td><?php echo $m->email ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Nama</th>
                      <th>Tempat Lahir</th>
                      <th>Tanggal Lahir</th>
                      <th>E-Mail</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>
    <?php $this->view('shared/footer')?>
    <?php $this->view('shared/script')?>
    <!-- page script -->
    <script>
      $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      })
    </script>
  </body>
  </html>