<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('shared/styles')?>
	<title>Sistem Pengujian | Rekap Hasil Pengujian Kepribadian</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Rekap Hasil
          <small>Pengujian Kepribadian Sifat</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Rekap"><i class="fa fa-table"></i> Rekap</a></li>
          <li class="ac">Kepribadian Sifat</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Data Rekap Hasil</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Tempat Lahir</th>
                      <th>Tanggal Lahir</th>
                      <th>Jawaban Positif</th>
                      <th>Jawaban Negatif</th>
                      <th>Kepribadian</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 0; 
                    foreach($uji_kepribadian as $item){
                        ?>
                        <tr>
                          <td><?php $no++; echo $no; ?></td>
                          <td><?php echo $item->nama?></td>
                          <td><?php echo $item->tempat_lahir?></td>
                          <td><?php echo $item->tanggal_lahir?></td>
                          <td><?php echo $item->jawaban_positif?></td>
                          <td><?php echo $item->jawaban_negatif?></td>
                          <td><?php echo $item->kepribadian?></td>
                        </tr>
                      <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Tempat Lahir</th>
                      <th>Tanggal Lahir</th>
                      <th>Jawaban</th>
                      <th>Kepribadian</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>
    <?php $this->view('shared/footer')?>
    <?php $this->view('shared/script')?>
    <!-- page script -->
    <script>
      $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      })
    </script>
  </body>
  </html>