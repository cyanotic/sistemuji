<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->view('shared/styles')?>
  <title>Sistem Pengujian | Pengujian Kepribadian</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pengujian Kepribadian
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Pengujian"><i class="fa fa-table"></i> Pengujian</a></li>
          <li class="ac">Kepribadian</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Gaya Manajemen (<label id="timer"></label>)</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form class="form-horizontal" id="form-kepribadian" method="post" action="<?php echo base_url()?>Pengujian/KepribadianSifatPost">
                  <input type="hidden" name="key" value="1">
                  <table id="example1" class="table">
                    <tbody>
                      <?php
                      $no = 0;
                      foreach($soal_kepribadian_gaya_manajemen as $item){
                        ?>
                        <tr>
                          <td><?php $no++; echo $no; ?></td>
                          <td><input type="radio" name="soal_<?php echo $item->id?>" value="a" class="minimal"></td>
                          <td>A. <?php echo $item->tipe_a ?></td>
                          <td><input type="radio" name="soal_<?php echo $item->id?>" value="b" class="minimal"></td>
                          <td>B. <?php echo $item->tipe_b ?></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="<?php echo base_url()?>Home" class="btn btn-default">Batal</a>
                      <a href="javascript:void(0)" class="btn btn-success" id="simpan">Simpan</a>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>

    <!-- Footer -->
    <?php $this->view('shared/footer')?>
    <!-- End Of Footer -->
  </div>

  <?php $this->view('shared/script')?>

  <script type="text/javascript">
    $('#timer').html("05:00");
    startTimer();

    function startTimer() {
      var presentTime = $('#timer').html();
      var timeArray = presentTime.split(/[:]+/);
      var m = timeArray[0];
      var s = checkSecond((timeArray[1] - 1));
      if(s==59){m=m-1}
        if(m<0){$('#form-kepribadian').submit()}

          $('#timer').html(m + ":" + s);
        setTimeout(startTimer, 1000);
      }

      $('#simpan').click(function(){
        if(confirm("Apakah Anda Yakin?"))
          $('#form-kepribadian').submit();            
      });

      function checkSecond(sec) {
        if (sec < 10 && sec >= 0) {sec = "0" + sec}; // add zero in front of numbers < 10
        if (sec < 0) {sec = "59"};
        return sec;
      }

      $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      })
    </script>

  </body>
  </html>