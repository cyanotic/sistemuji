<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user = $this->db->get('member',array('username' => $this->session->username))->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('shared/styles')?>
	<title>Sistem Pengujian | Pengujian Kepribadian</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Header Navbar -->
    <?php $this->view('shared/navbar')?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->view('shared/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pengujian Kepribadian
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>Pengujian"><i class="fa fa-table"></i> Pengujian</a></li>
          <li class="ac">Kepribadian</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Kuisioner I (<label id="timer"></label>)</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form class="form-horizontal" id="form-kepribadian" method="post" action="<?php echo base_url()?>Pengujian/KepribadianSifatPost">
                  <input type="hidden" name="key" value="1">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th colspan="2" class="text-center">A</th>
                        <th colspan="2" class="text-center">B</th>
                        <th colspan="2" class="text-center">C</th>
                        <th colspan="2" class="text-center">D</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 0;
                      foreach($soal as $item){
                        ?>
                        <tr>
                          <td><?php $no++; echo $no; ?></td>
                          <td><input type="radio" name="soal_<?php echo $item->id?>" value="a" class="minimal"></td>
                          <td><?php echo $item->tipe_a ?></td>
                          <td><input type="radio" name="soal_<?php echo $item->id?>" value="b" class="minimal"></td>
                          <td><?php echo $item->tipe_b ?></td>
                          <td><input type="radio" name="soal_<?php echo $item->id?>" value="c" class="minimal"></td>
                          <td><?php echo $item->tipe_c ?></td>
                          <td><input type="radio" name="soal_<?php echo $item->id?>" value="d" class="minimal"></td>
                          <td><?php echo $item->tipe_d ?></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th colspan="2" class="text-center">A</th>
                        <th colspan="2" class="text-center">B</th>
                        <th colspan="2" class="text-center">C</th>
                        <th colspan="2" class="text-center">D</th>
                      </tr>
                    </tfoot>
                  </table>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="<?php echo base_url()?>Home" class="btn btn-default">Batal</a>
                      <a href="javascript:void(0)" class="btn btn-success" id="simpan">Simpan</a>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>

    <!-- Footer -->
    <?php $this->view('shared/footer')?>
    <!-- End Of Footer -->
  </div>
  <?php $this->view('shared/script')?>
  <script type="text/javascript">
    $('#timer').html("05:00");
    startTimer();

    function startTimer() {
      var presentTime = $('#timer').html();
      var timeArray = presentTime.split(/[:]+/);
      var m = timeArray[0];
      var s = checkSecond((timeArray[1] - 1));
      if(s==59){m=m-1}
        if(m<0){$('#form-kepribadian').submit()}

          $('#timer').html(m + ":" + s);
        setTimeout(startTimer, 1000);
      }

      $('#simpan').click(function(){
        if(confirm("Apakah Anda Yakin?"))
          $('#form-kepribadian').submit();            
      });

      function checkSecond(sec) {
        if (sec < 10 && sec >= 0) {sec = "0" + sec}; // add zero in front of numbers < 10
        if (sec < 0) {sec = "59"};
        return sec;
      }
    </script>
  </body>
  </html>