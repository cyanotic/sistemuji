<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->model('UserModel');
	}

	public function index()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 2)
			redirect('Home/Index');
		$data['member'] = $this->m_data->tampil_data()->result();
		$this->load->view('member/index',$data);
	}

	public function tampil()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 2)
			redirect('Home/Index');

		$data['member'] = $this->m_data->tampil_data()->result();
		$this->load->view('member/tampil_member',$data);
	}
	
	public function tambah()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 2)
			redirect('Home/Index');

		$this->load->view('member/tambah');
	}

	public function tambahpost(){
		if(is_null($this->input->post('username')) || is_null($this->input->post('password')))
			redirect('Member/Tambah');

		$nama = $this->input->post('nama');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = array(
			'nama' => $nama,
			'tempat_lahir' => $tempat_lahir,
			'tanggal_lahir' => $tanggal_lahir,
			'email' => $email,
			'username' => $username,
			'password' => $password,
			'passhashed' => md5($password),
			'id_role' => 2
		);
		$this->m_data->input_data($data,'member');
		redirect('Member/index');
	}

	public function edit($memberUser = null){
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 2)
			redirect('Home/Index');

		$member = $this->UserModel->GetUserData($memberUser)->row();
		if(!isset($member) || $member->id_role == 1)
			redirect('Member/Index');

		$data['member'] = $member;
		$this->load->view('member/edit',$data);
	}
}
