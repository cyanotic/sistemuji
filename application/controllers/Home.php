<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->model('UserModel');
		$this->load->model('PengujianModel');
	}

	public function index()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();

		if($user->id_role == 1){
			$data['grafik']=$this->PengujianModel->GetUjiKepribadianSifat();
			$this->load->view('home/index_admin',$data);
		}elseif($user->id_role == 2){
			$data['member'] = $this->m_data->tampil_data()->result();
			$this->load->view('home/index',$data);
		}
	}
}
