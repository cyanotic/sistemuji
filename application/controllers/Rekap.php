<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('UserModel');
		$this->load->model('RekapModel');
	}

	public function kepribadian($tipe = "Sifat")
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 2)
			redirect('Home/Index');

		if(strtolower($tipe) == "sifat"){
			$data['uji_kepribadian'] = $this->RekapModel->RekapUjiKepribadianSifat()->result();
			$this->load->view('rekap/kepribadiansifat',$data);
		}
	}

	public function kecerdasan()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->db->query("SELECT * FROM member WHERE username = '$username'")->row();
		if($user->id_role == 2)
			redirect('Home/Index');

		$this->load->view('rekap/kecerdasan');
	}
}
