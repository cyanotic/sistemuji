<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('UserModel');
	}

	public function login()
	{
		if(isset($this->session->username))
			redirect('Home/Index');
		$this->load->view('account/login');
	}

	public function loginpost()
	{
		if(is_null($this->input->post('user')) || is_null($this->input->post('password')))
			redirect('Account/Login');

		$user = $this->input->post('user');
		$pass = $this->input->post('password');
		$getUser = $this->UserModel->CheckLogin($user,$pass);
		if($getUser){
			$this->session->username = $getUser->username;

			redirect("Home/Index");
		}

		redirect('Account/Login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("Account/Login");
	}
}
