<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengujian extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('PengujianModel');
		$this->load->model('UserModel');
	}

	public function index()
	{
		redirect('Home/Index');
	}

	public function kepribadiansifat($key = 1)
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 1)
			redirect('Home/Index');

		if($this->PengujianModel->UserKepribadian($username))
			redirect('Home/Index');
		if($key == 1){
			$data['soal'] = $this->PengujianModel->GetSoalKepribadian(1)->result();
			$this->load->view('pengujian/kepribadiansifat1',$data);
		} elseif($key == 2){
			if(is_null($this->session->flashdata('jawaban')))
				redirect('Pengujian/KepribadianSifat/1');
			$data['jawaban'] = $this->session->flashdata('jawaban');
			$data['soal'] = $this->PengujianModel->GetSoalKepribadian(2)->result();
			$this->load->view('pengujian/kepribadiansifat2',$data);
		} else
		redirect('Home/Index');
	}


	public function kepribadianDayaJuang()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 1)
			redirect('Home/Index');

		if($this->PengujianModel->UserKepribadian($username))
			redirect('Home/Index');

		$data['soal_kepribadian_daya_juang'] = $this->PengujianModel->GetSoalKepribadianDayaJuang()->result();
		$this->load->view('pengujian/kepribadian_daya_juang',$data);

	}

	public function kepribadianGayaKepemimpinan()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 1)
			redirect('Home/Index');

		if($this->PengujianModel->UserKepribadian($username))
			redirect('Home/Index');

		$data['soal_kepribadian_gaya_kepemimpinan'] = $this->PengujianModel->GetSoalKepribadianGayaKepemimpinan()->result();
		$this->load->view('pengujian/kepribadian_gaya_kepemimpinan',$data);
	}

	public function kepribadianGayaManajemen()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 1)
			redirect('Home/Index');

		if($this->PengujianModel->UserKepribadian($username))
			redirect('Home/Index');

		$data['soal_kepribadian_gaya_manajemen'] = $this->PengujianModel->GetSoalKepribadianGayaManajemen()->result();
		$this->load->view('pengujian/kepribadian_gaya_manajemen',$data);
	}

	public function kecerdasan()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 1)
			redirect('Home/Index');

		if($this->PengujianModel->UserKepribadian($username))
			redirect('Home/Index');

		$data['soal_kecerdasan'] = $this->PengujianModel->GetSoalKecerdasan()->result();
		$this->load->view('pengujian/kecerdasan_1',$data);
	}

	public function kepribadiansifatpost()
	{
		if(is_null($this->session->username))
			redirect('Account/Login');
		$username = $this->session->username;
		$user = $this->UserModel->GetUserData($username)->row();
		if($user->id_role == 1)
			redirect('Home/Index');

		if($this->input->post('key') == 1){
			$jawab = "";
			$ans = ["a","b","c","d"];

			for($i = 0; $i <20 ; $i++){
				$no = $i + 1;
				$jawaban = $this->input->post('soal_'.$no); 
				$jawab .= $jawaban;
				if(!in_array($jawaban, $ans))
					$jawab .= "-";
			}

			$this->session->set_flashdata('jawaban',$jawab);
			redirect('Pengujian/KepribadianSifat/2');
		} else{
			$jawab = "";
			$ans = ["a","b","c","d"];
			$prevjawab = $this->input->post('jawaban');

			for($i = 0; $i <20 ; $i++){
				$no = $i + 1;
				$jawaban = $this->input->post('soal_'.$no); 
				$jawab .= $jawaban;
				if(!in_array($jawaban, $ans))
					$jawab .= "-";
			}

			$jumlah_a = substr_count($jawab, 'a') + substr_count($prevjawab, 'a');
			$jumlah_b = substr_count($jawab, 'b') + substr_count($prevjawab, 'b');
			$jumlah_c = substr_count($jawab, 'c') + substr_count($prevjawab, 'c');
			$jumlah_d = substr_count($jawab, 'd') + substr_count($prevjawab, 'd');

			$id_kepribadian = $this->PengujianModel->GetKepribadian($jumlah_a,$jumlah_b,$jumlah_c,$jumlah_d);

			$data = array(
				"username" => $username,
				"jawaban_positif" => $prevjawab,
				"jawaban_negatif" => $jawab,
				"jumlah_a" => $jumlah_a,
				"jumlah_b" => $jumlah_b,
				"jumlah_c" => $jumlah_c,
				"jumlah_d" => $jumlah_d,
				"id_kepribadian" => $id_kepribadian
			);

			$this->PengujianModel->InsertUjiKepribadian($data);
			redirect('Home/Index');
		}
	}

	public function blank(){
		$this->load->view('shared/blank');
	}
}
